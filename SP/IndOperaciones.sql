IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[DBO].[BITACORAACCESOALT]') AND type IN ('P'))
	DROP PROCEDURE SP_AltaIndicadores
GO

CREATE PROCEDURE SP_AltaIndicadores(
	------------------------------------------------------------------------------------------------------------------
	-- @nombre:			SP_AltaIndicadores
	-- @autor:			DINERO INMEDIATO
	-- @fecha:			05/04/2021
	-- @version:		1.0
	-- @descripcion:	Store procedure para dar de alta los indicadores de gestion de operaciones en la tabla IndicadoresOperaciones
	------------------------------------------------------------------------------------------------------------------|
	
	@Par_ErrMenDev 				VARCHAR(800) OUT		-- Parametro de entrada/salida de mensaje de control de respuesta 
)
AS

BEGIN

-- Declaracion de constantes 
	DECLARE @ENTERO_VACIO		INT,				-- Entero vacio
			@CADENA_VACIA		CHAR(1),			-- Cadena vacia
			@DECIMAL_VACIO		DECIMAL(9,2),		-- Cadena vacia
			@FECHA_VACIA		DATETIME,			-- Fecha vacia
			@SalidaSI			CHAR(1),			-- Salida si
			@SalidaNO			CHAR(1),			-- Salida no
			@CodigoExito		CHAR(6) 			-- Codigo exito			
	
		-- Declaracion de variables 
	DECLARE	@Var_TablaID		INT,					-- FOLIO ID
			@Var_Fecha			DATETIME,
			@Var_FechaActual	DATETIME,
			@Var_Nombre			VARCHAR(400),
			@Var_Real			DECIMAL(14, 2),
			@Var_Registro		INT
					
-- Asignacion de constantes 
	SELECT	@ENTERO_VACIO		=	0,				-- Asignacion de entero vacio
			@CADENA_VACIA		=	'Todos',		-- Asignacion de cadena vacia
			@DECIMAL_VACIO		=	0000000.00,		-- Asinacion de decimal vacio
			@FECHA_VACIA		=	'1900-01-01',	-- Asignacion de fecha vacia
			@SalidaSI			=	'S',			-- Asignacion de salida si
			@SalidaNO			=	'N',			-- Asignacion de salida no
			@CodigoExito		=	'000000'		-- Codigo exito
	
	BEGIN TRAN IND
	
	BEGIN TRY
		
		SELECT @Var_Registro = (SELECT COUNT(IndicadorID) FROM IndOperaciones
									WHERE CONVERT(DATE, Fecha)= CONVERT(DATE,GETDATE()))
	
		
		IF(@Var_Registro = 0 )
		BEGIN
			SELECT @Var_FechaActual= GETDATE()
			
			---PRESTAMO PROMEDIO 
			
			INSERT INTO IndOperaciones (Nombre, Real, Fecha, SucursalID, StsEnvio, RegVersion) 
			SELECT ('Prestamo promedio'),(ISNULL(SUM(Capital)/ COUNT(FolOri),0)),@Var_FechaActual, 'Centro 69', 0,0
							FROM dbo.vwEmpMovttoGral
							WHERE  FecIni <=(SELECT DATEADD(d,-1,GETDATE()))
							AND AnnEmp = YEAR(GETDATE()) 
							AND MesEmp=MONTH(GETDATE())
							AND Status<>'Cancelada'	 
			
			--FOLIOS DE PRESTAMO PROMEDIO
			
			INSERT INTO IndOperaciones (Nombre, Real, Fecha, SucursalID, StsEnvio, RegVersion) 
				SELECT	('Folios prestamo promedio'), ISNULL(COUNT(FolOri),0) AS Folios, @Var_FechaActual, 'Centro 69', 0,0
							FROM dbo.vwEmpMovttoGral
							WHERE  FecIni <=(SELECT DATEADD(d,-1,GETDATE()))
							AND AnnEmp = YEAR(GETDATE()) 
							AND MesEmp=MONTH(GETDATE())
							AND Status<>'Cancelada'
					
			---PORCENTAJE MAXIMO ENAJENACION
			
			INSERT INTO IndOperaciones (Nombre, Real, Fecha, SucursalID, StsEnvio, RegVersion) 
			SELECT TOP 1 'Porcentaje m�ximo de enajenaci�n', Folios ,@Var_FechaActual, 'Centro 69', 0,0
				FROM dbo.vwIndEnajenados3
				WHERE 	AnnMov=YEAR(GETDATE()) 
				AND MesMov=MONTH(GETDATE())
				ORDER BY DiaMov DESC
						
			---CARTERA TOTAL
			
			SELECT @Var_Fecha= (SELECT DATEADD(dd, DATEPART(dd, GETDATE())* -1,  GETDATE()))
			
			INSERT INTO IndOperaciones (Nombre, Real, Fecha, SucursalID, StsEnvio, RegVersion) 
			SELECT 'Cartera total', SUM(CapitalVigente) ,@Var_FechaActual, 'Centro 69', 0,0
				FROM [vwIndCartera] 
				WHERE AnnCar=YEAR(@Var_Fecha) 
				AND MesCar=MONTH(@Var_Fecha) 
				AND DiaCar=DAY(@Var_Fecha)
					
			---CAPITAL PRESTADO
			
			INSERT INTO IndOperaciones (Nombre, Real, Fecha, SucursalID, StsEnvio, RegVersion) 
			SELECT 'Capital prestado',ISNULL(SUM(capital), 0) ,@Var_FechaActual, 'Centro 69', 0,0 
				FROM [Empe�o].[dbo].[vwEmpMovttoCartera] 
				WHERE CONVERT(DATETIME,CAST(DiaEmp AS VARCHAR(200)) +'/'+ CAST(MesEmp AS VARCHAR(200)) +'/'+CAST(annEmp AS VARCHAR(200)), 103)
				<=(SELECT DATEADD(d,-1,GETDATE()))
				AND AnnEmp = YEAR(GETDATE()) AND MesEmp=MONTH(GETDATE()) AND Status<>'Cancelada'
					
			--PROMEDIO DE PRESTAMO CLIENTES NUEVOS:
			
			INSERT INTO IndOperaciones (Nombre, Real, Fecha, SucursalID, StsEnvio, RegVersion) 
			SELECT  'Promedio de pr�stamo clientes nuevos',ISNULL(SUM(Capital) / COUNT(*), 0.00),@Var_FechaActual, 'Centro 69', 0,0 
				FROM     vwEmpMovttoGral
				WHERE  (CveCli IN
					   (SELECT  CVECLI
							FROM     ListaClientesNuevos AS Lista
							WHERE  (YEAR(FECALT) = YEAR(GETDATE())) AND (MONTH(FECALT) = MONTH(GETDATE()))
							AND (vwEmpMovttoGral.AnnEmp = year(GETDATE())) AND (vwEmpMovttoGral.MesEmp = month(GETDATE()))))
			
			--CLIENTES NUEVOS EMPE�OS
			
			INSERT INTO IndOperaciones (Nombre, Real, Fecha, SucursalID, StsEnvio, RegVersion) 
			SELECT 'Clientes nuevos empe�os', COUNT(distinct CveCli),@Var_FechaActual, 'Centro 69', 0,0 
				FROM     vwEmpMovttoGral
				WHERE  (CveCli IN
						(SELECT  CVECLI
								FROM     ListaClientesNuevos AS Lista
								WHERE  (YEAR(FECALT) = YEAR(GETDATE())) 
								AND (MONTH(FECALT) = MONTH(GETDATE()))
								AND (vwEmpMovttoGral.AnnEmp = YEAR(GETDATE())) 
								AND (vwEmpMovttoGral.MesEmp = MONTH(GETDATE()))))
							
			--CLIENTES NUEVOS VENTAS
			--ESTA EN SICAVI SERVIDOR 4
			
			--INGRESOS (EMPE�O)
			
			INSERT INTO IndOperaciones (Nombre, Real, Fecha, SucursalID, StsEnvio, RegVersion) 
			SELECT 'Ingresos (empe�o)', ISNULL(SUM(Interes + Recargos + PagReimp),0),@Var_FechaActual, 'Centro 69', 0,0
			FROM     empe�o.dbo.vwEmpMovtosResumenPagos				
			WHERE  (AnnPag = YEAR(getdate())) AND (MesPag = MONTH(getdate()))
			and convert(datetime,cast(DiaPag as varchar(200)) +'/'+ cast(MesPag as varchar(200)) +'/'+cast(AnnPag as varchar(200)), 103)
			<=(SELECT DATEADD(d,-1,GETDATE()))
			
			--CARTERA APARATOS, CELULAR Y JOYERIA
			 
			INSERT INTO IndOperaciones (Nombre, Real, Fecha, SucursalID, StsEnvio, RegVersion) 
				SELECT CASE Tipo
						When 'Aparatos' Then 'Aparatos'
						When 'Celulares' Then 'Celulares'
						When 'Joyeria' Then 'Joyer�a'
						END, CapitalVigente, @Var_FechaActual, 'Centro 69',0,0
					FROM [vwIndCartera]
					WHERE AnnCar=YEAR(GETDATE()) 
					AND MesCar=month(GETDATE()) --and DiaCar=day(GETDATE())
					AND DiaCar=day((SELECT DATEADD(d,-1,GETDATE()))) 
			
			
			
			SET @Par_ErrMenDev = 'Se realiz� la alta con exito.'
			COMMIT TRAN IND
						
		END
		ELSE
		BEGIN
			PRINT 'YA SE HAN REGISTRADO LOS INDICADORES DEL DIA DE HOY'
		END
	END TRY
	BEGIN CATCH
	
			SET @Par_ErrMenDev = 'Ocurrio un Error: ' + ERROR_MESSAGE() + ' en la l�nea ' + CONVERT(NVARCHAR(255), ERROR_LINE() ) + '.'
		    Rollback TRAN IND
	END CATCH
	
END
