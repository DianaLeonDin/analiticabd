IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[DBO].[EMPLEADOSUCURSALALT]') AND type IN ('P'))
	DROP PROCEDURE EMPLEADOSUCURSALALT
GO

CREATE PROCEDURE EMPLEADOSUCURSALALT(
	------------------------------------------------------------------------------------------------------------------
	-- @nombre:			EMPLEADOSUCURSALALT
	-- @autor:			DINERO INMEDIATO
	-- @fecha:			31/03/2021
	-- @version:		1.0
	-- @descripcion:	Store procedure para dar de alta los permisos de empleados cual sucursal les corresponde,en los indicadores de gestion de operaciones
	------------------------------------------------------------------------------------------------------------------|
	
	@Par_Sucursal				VARCHAR(300),
	@Par_SucursalID				VARCHAR(300),
	@Par_UsuarioID				VARCHAR(300),
	
	@Par_Salida					CHAR(1),				-- Parametro para salida de datos
	@Par_NumErr					CHAR(6) OUT,			-- Parametro de entrada/salida de numero de error
	@Par_ErrMenDev 				VARCHAR(800) OUT		-- Parametro de entrada/salida de mensaje de control de respuesta 
	--@Par_Consecutivo			INT	OUT				    -- Parametro de entrada/salida de para indicar el id que se a generado o actualizado	
)
AS

BEGIN

-- Declaracion de constantes 
	DECLARE @ENTERO_VACIO		INT,				-- Entero vacio
			@CADENA_VACIA		CHAR(1),			-- Cadena vacia
			@DECIMAL_VACIO		DECIMAL(9,2),			-- Cadena vacia
			@FECHA_VACIA		DATETIME,			-- Fecha vacia
			@SalidaSI			CHAR(1),			-- Salida si
			@SalidaNO			CHAR(1),			-- Salida no
			@CodigoExito		CHAR(6) 			-- Codigo exito			
	
		-- Declaracion de variables 
	DECLARE	@Var_TablaID		INT,					-- FOLIO ID
			@Var_BaseReg		INT,					-- Se verifica si el cliente ya tiene una base
			@Var_MontoBase		DECIMAL(14,2),
			@Var_Fecha			DATETIME
		
   
   
-- Asignacion de constantes 
	SELECT	@ENTERO_VACIO		=	0,				-- Asignacion de entero vacio
			@CADENA_VACIA		=	'Todos',				-- Asignacion de cadena vacia
			@DECIMAL_VACIO		=	0000000.00,		-- Asinacion de decimal vacio
			@FECHA_VACIA		=	'1900-01-01',	-- Asignacion de fecha vacia
			@SalidaSI			=	'S',			-- Asignacion de salida si
			@SalidaNO			=	'N',			-- Asignacion de salida no
			@CodigoExito		=	'000000'		-- Codigo exito

	
	
	SET	@Par_SucursalID						= ISNULL(@Par_SucursalID			,@CADENA_VACIA)		
	SET	@Par_Sucursal						= ISNULL(@Par_Sucursal				,@CADENA_VACIA)		
	SET	@Par_UsuarioID					    = ISNULL(@Par_UsuarioID				,@CADENA_VACIA)	

		
	IF @Par_SucursalID		=   @CADENA_VACIA	
	BEGIN
		SET	@Par_NumErr		= '000001'
		SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA EL FOLIO DE LA SUCURSAL'
		GOTO ManejoErrores	
	END
	
	IF @Par_Sucursal		=   @CADENA_VACIA	
	BEGIN
		SET	@Par_NumErr		= '000002'
		SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA LA SUCURSAL'
		GOTO ManejoErrores	
	END
	
	IF @Par_UsuarioID				=   @CADENA_VACIA	
	BEGIN
		SET	@Par_NumErr		= '000003'
		SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA EL USUARIO'
		GOTO ManejoErrores	
	END
	
	
	SELECT @Var_Fecha = GETDATE()
					
	INSERT INTO PermisoOperaciones(SucursalID,					Sucursal,			UsuarioID) 
							VALUES (@Par_SucursalID,			@Par_Sucursal,		@Par_UsuarioID)
	
	SET @Par_NumErr  	= '000000'
	SET @Par_ErrMenDev	= 'SE HA REGISTRADO EL PERMISO CON �XITO'
	
	
	ManejoErrores:
			
	IF @Par_Salida	= @SalidaSI
	BEGIN

	
		SELECT	@Par_NumErr 		AS NumErr,
				@Par_ErrMenDev		AS ErrMenDev
				--@Par_Consecutivo	AS Consecutivo
	END
	

END
