IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[DBO].[MANTOPERACIONESCON]') AND type IN ('P'))
	DROP PROCEDURE MANTOPERACIONESCON
GO

CREATE PROCEDURE MANTOPERACIONESCON(
	------------------------------------------------------------------------------------------------------------------
	-- @nombre:			MANTOPERACIONESCON
	-- @autor:			DINERO INMEDIATO
	-- @fecha:			12/01/2021
	-- @version:		1.0
	-- @descripcion:	Store procedure para consultar informacion de los catalogos de mantenimieno (para la consulta de la base)
	------------------------------------------------------------------------------------------------------------------|
		
	@Par_NumCon			INT,					-- Numero de lista
	@Par_FolioID		INT,					-- Numero de la base
	@Par_SucursalID		VARCHAR(300),			-- Numero de la base
	
	@Par_Nombre			VARCHAR(300),			-- Nombre del indicador
	
	@Par_Mes			INT,					--Filtrar por mes anterior
	@Par_Anio			INT						--Filtrar por a�o anterior
)
AS
BEGIN
	
	-- Declaracion de constantes 
	DECLARE @ENTERO_VACIO			INT,				-- Entero vacio
			@CADENA_VACIA			CHAR(1),			-- Cadena vacia
			@Var_ConBase			INT,
			@Var_ConDiaHabil		INT,
			@Var_ConFolioDH			INT,
			@Var_ConDHabilMes		INT,				--dias habiles con el mes y a�o como parametros
			@Var_ConComentario		INT	
			
	-- Declaracion de variables 
	DECLARE	@Var_FechaActual	DATETIME

	-- Asignacion de constantes 
	SELECT	@ENTERO_VACIO			=	0,				-- Asignacion de entero vacio
			@CADENA_VACIA			=	'',				-- Asignacion de cadena vacia
			@Var_ConBase			=	1,				-- Consltar base del catalogo de mantenimiento
			@Var_ConDiaHabil		=	2,
			@Var_ConFolioDH			=	3,				-- Consulta por medio de dias habiles por medio del folioID del dia habil
			@Var_ConDHabilMes		=	4,
			@Var_ConComentario		=	5				--comentarios de los indicadores diarios
			
	
	---------------	 *** INDICADOR DE MATRIZ DE INDICADORES DE GESTION(DIARIO)
	
	
	--consulta de la base en mantenimiento (Matriz de indicadores de gestion para operaciones)
	IF @Par_NumCon = @Var_ConBase
	BEGIN	      			
		
		
		SELECT BaseID, NomIndicador, ValBase, SucursalID, --ISNULL(AliadoID, 'Todos')AS AliadoID, --Mes,
			CONVERT(VARCHAR(20),anio) +'-'+ RIGHT('00' + Ltrim(Rtrim(Mes)),2) AS NomMes,
			ISNULL((select e.Em_Nombre FROM DBDIN.dbo.Empleado e where e.Em_UserDef_1 = BASE.AliadoID), 'Todos')AS AliadoID
				  FROM BASE WHERE BaseID= @Par_FolioID
				  
		--SELECT BaseID, NomIndicador, ValBase, SucursalID, ISNULL(AliadoID, 'Todos')AS AliadoID, --Mes,
			--anio, Mes,
			--convert(varchar(30), anio)+'-'+ convert(varchar(30),(REPLICATE('0',(2-len(mes)))+Mes))
			--FROM BASE WHERE BaseID= 2@Par_FolioID

		
	END	
	
	--TOTAL Dias habiles por a�o y mes actual(pantalla principal operaciones)
	IF	@Par_numCon =	@Var_ConDiaHabil
	BEGIN
	
		--SELECT DATEDIFF(DD, GETDATE(), DATEADD(MM, 1, GETDATE())) - count(month(GETDATE()))AS Cantidad
				--FROM dbo.CatDiasIn
				--WHERE YEAR(Fecha)= YEAR(GETDATE())
				--AND MONTH(Fecha)= MONTH(GETDATE())

		
		--SELECT DATEDIFF(dd, GETDATE(), DATEADD(mm, 1, GETDATE()))- COUNT(FechaInhabil) AS Cantidad
		--	FROM DIASHABILES WHERE 
		--	A�o= YEAR(GETDATE())
		--	AND Mes= MONTH(GETDATE())
		--	AND SucursalID = @Par_SucursalID
		--	-- pra buscar las fechas que han pasado, comtemplando la fecha actual
		--	AND FechaInhabil <GETDATE()
		
		SELECT DATEDIFF(dd, GETDATE(), DATEADD(mm, 1, GETDATE()))- COUNT(FechaInhabil) AS DiasHabiles,
				(SELECT 
					DAY(GETDATE()) - COUNT(FechaInhabil) AS DiasTranscurridos FROM DIASHABILES WHERE 
					A�o= YEAR(GETDATE())
					AND Mes= MONTH(GETDATE())
					AND SucursalID = @Par_SucursalID
					AND 
					FechaInhabil <GETDATE())DiasTranscurridos
			FROM DIASHABILES WHERE 
			A�o= YEAR(GETDATE())
			AND Mes= MONTH(GETDATE())
			AND SucursalID = @Par_SucursalID

	END
	
	--Dia habile por folioid ---MODIFICAR DIAS HABILES, SE BUSCA 
	IF	@Par_numCon =	@Var_ConFolioDH
	BEGIN
			
		--SELECT FolioID,SucursalID, Cantidad ,
		--	CONVERT(VARCHAR(20),A�o) +'-'+ RIGHT('00' + Ltrim(Rtrim(Mes)),2) AS Fecha
		--	FROM DIASHABILES
		--	WHERE FolioID = @Par_FolioID
		SELECT SucursalID, 
			CONVERT(VARCHAR(20),A�o) +'-'+ RIGHT('00' + Ltrim(Rtrim(Mes)),2) AS Fecha
			--FechaInhabil
			FROM DIASHABILES
			WHERE SucursalID= @Par_SucursalID
			and A�o= @Par_Anio and Mes = @Par_Mes
			group by SucursalID, A�o, Mes 
		
		
	END
	
	---------------	 *** INDICADOR DE MATRIZ DE INDICADORES DE GESTION(DEL MES ANTERIOR)
	
	--TOTAL Dias habiles por a�o y mes anterior (pantalla principal operaciones)
	IF	@Par_numCon =	@Var_ConDHabilMes
	BEGIN
		
		--SELECT DATEDIFF(dd, GETDATE(), DATEADD(mm, 1, GETDATE()))- COUNT(FechaInhabil) AS Cantidad
		--	FROM DIASHABILES WHERE 
		--	A�o= @Par_Anio
		--	AND Mes= @Par_Mes
		--	AND SucursalID = @Par_SucursalID
		SELECT @Var_FechaActual = (SELECT DATEADD(dd, DATEPART(dd, GETDATE())* -1,  GETDATE()))
		
		SELECT DAY(DATEADD(DD,-1,
			DATEADD(MM,DATEDIFF(MM,-1,CONVERT(DATE,CONVERT(VARCHAR(50),YEAR(@Var_FechaActual))+'/'+CONVERT(VARCHAR(50),MONTH(@Var_FechaActual))+'/'+CONVERT(VARCHAR(50),1))),0)))
			- COUNT(FechaInhabil) AS DiasHabiles, CONVERT(INT,0) AS DiasTranscurridos
			FROM DIASHABILES WHERE
			A�o= YEAR(@Var_FechaActual)
			AND Mes= MONTH(@Var_FechaActual)
			AND SucursalID= @Par_SucursalID

	END
	
	
		------comentarios en los modales de los indicadores diarios
	IF	@Par_numCon =	@Var_ConComentario
	BEGIN
		
		--SELECT Descripcion, FechaAlta 
		--	FROM AnaliticaInd.dbo.COMENTARIO 
		--	WHERE BaseID = 289
		
			SELECT c.Descripcion, c.FechaAlta, b.NomIndicador
				FROM AnaliticaInd.dbo.COMENTARIO  as c
				INNER JOIN AnaliticaInd.dbo.BASE as b
				ON c.BaseID = b.BaseID
				WHERE b.NomIndicador = @Par_Nombre
				--b.NomIndicador='Prestamo promedio'
				AND MONTH(c.FechaAlta)= MONTH(GETDATE())
				AND YEAR(c.FechaAlta)=YEAR(GETDATE())
				AND c.Sucursal =@Par_SucursalID
		


	END
	
	
	
END
