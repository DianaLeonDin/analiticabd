IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[MANOPERACIONESACT]') AND type IN ('P'))
	DROP PROCEDURE dbo.MANOPERACIONESACT
GO

CREATE PROC MANOPERACIONESACT(
	------------------------------------------------------------------------------------------------------------------
	-- @nombre:			MANOPERACIONESACT
	-- @autor:			DINERO INMEDIATO
	-- @fecha:			14/01/2021
	-- @version:		1.0
	-- @descripcion:	Store procedure para actualizar los datos de  mantenimiento de los indicadores de operaciones. 
	------------------------------------------------------------------------------------------------------------------|
	
	@Par_FolioID					INT,				-- id del credito que se dara de baja
	@Par_Nombre						CHAR(200),
	@Par_Meta						DECIMAL(9,2),
	@Par_SucursalID					CHAR(200),
	@Par_Aliado						CHAR(300),
	@Par_MesAnio					CHAR(200),
	
	@Par_NumAct						INT,
	@Par_Salida						CHAR(1),			-- Parametro para salida de datos
	@Par_NumErr						CHAR(6) OUT,		-- Parametro de entrada/salida de numero de error
	@Par_ErrMenDev 					VARCHAR(800) OUT,	-- Parametro de entrada/salida de mensaje de respuesta
	@Par_Consecutivo				BIGINT	OUT			-- Parametro de entrada/salida de para indicar el id que se a generado o actualizado	
	
)

AS
BEGIN
-- Declaracion de constantes 
	DECLARE @ENTERO_VACIO		INT,				-- Entero vacio
			@CADENA_VACIA		CHAR(1),			-- Cadena vacia
			@DECIMAL_VACIA		DECIMAL(9,2),			-- Decimal vacio
			@FECHA_VACIA		DATETIME,			-- Fecha vacia
			@SalidaSI			CHAR(1),			-- Salida si
			@SalidaNO			CHAR(1),			-- Salida no
			@CodigoExito		CHAR(6),			-- Codigo exito
			@Var_Act_Base		INT					-- numero de actualizacion de la base/meta del indicador de operaciones
		
	DECLARE @Var_Documento		INT,					-- aCT 1 Servira para verificar existencia del registro
			@Var_FechaActual	DATETIME,
			@Var_Mes			INT,
			@Var_Anio			INT,
			@Var_DiaHabil		INT,
			@Var_NomMes			VARCHAR(100),
			@Var_AnioV			VARCHAR(50)
						
	-- Asignacion de constantes 
	SELECT	@ENTERO_VACIO		=	0,				-- Asignacion de entero vacio
			@CADENA_VACIA		=	'',				-- Asignacion de cadena vacia
			@DECIMAL_VACIA		=	0.00,
			@FECHA_VACIA		=	'1900-01-01',	-- Asignacion de fecha vacia
			@SalidaSI			=	'S',			-- Asignacion de salida si
			@SalidaNO			=	'N',			-- Asignacion de salida no
			@CodigoExito		=	'000000',		-- Codigo exito
			@Var_Act_Base		= 	1,
			@Var_DiaHabil		=	2

	-- Actualizacion de la base de la matriz de indicador de gestiones en operaciones 
	IF @Par_NumAct = @Var_Act_Base
	BEGIN
	
		SET @Par_FolioID		=  ISNULL(@Par_FolioID,@ENTERO_VACIO)		
		IF @Par_FolioID			= @ENTERO_VACIO	
		BEGIN
			SET	@Par_NumErr		= '000001'
			SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA EL FOLIO DE LA BASE'
			GOTO ManejoErrores	
		END
		
		SET @Par_Nombre		=  ISNULL(@Par_Nombre,@CADENA_VACIA)		
		IF @Par_Nombre			= @CADENA_VACIA	
		BEGIN
			SET	@Par_NumErr		= '000002'
			SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA EL NOMBRE DE LA BASE'
			GOTO ManejoErrores	
		END
		
		SET @Par_Meta		=  ISNULL(@Par_Meta,@DECIMAL_VACIA)		
		IF @Par_Meta			= @DECIMAL_VACIA	
		BEGIN
			SET	@Par_NumErr		= '000003'
			SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA LA META DE LA BASE'
			GOTO ManejoErrores	
		END
		
		SET @Par_SucursalID		=  ISNULL(@Par_SucursalID,@CADENA_VACIA)		
		IF @Par_SucursalID			= @CADENA_VACIA	
		BEGIN
			SET	@Par_NumErr		= '000004'
			SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA LA SUCURSAL DE LA BASE'
			GOTO ManejoErrores	
		END
		
		SET @Par_Aliado		=  ISNULL(@Par_Aliado,@CADENA_VACIA)		
		IF @Par_Aliado			= @CADENA_VACIA	
		BEGIN
			SET	@Par_NumErr		= '000005'
			SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA EL ALIADO DE LA BASE'
			GOTO ManejoErrores	
		END
		
		SET @Par_MesAnio		=  ISNULL(@Par_MesAnio,@CADENA_VACIA)		
		IF @Par_MesAnio			= @CADENA_VACIA	
		BEGIN
			SET	@Par_NumErr		= '000006'
			SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA LA FECHA DE LA BASE'
			GOTO ManejoErrores	
		END


		SELECT @Var_Anio=CAST(SUBSTRING(@Par_MesAnio, 1, 4) AS INT)
		SELECT @Var_Mes= CAST(SUBSTRING(@Par_MesAnio, 6, 2)AS INT )
		
	
		--SELECT * FROM AnaliticaInd.DBO.BASE
		UPDATE AnaliticaInd.DBO.BASE 
			SET NomIndicador = @Par_Nombre, 
			ValBase = @Par_Meta, 
			SucursalID = @Par_SucursalID, 
			AliadoID = @Par_Aliado, 			
			Mes = @Var_Mes, 
			anio = @Var_Anio
			WHERE
			BaseID = @Par_FolioID
			AND Activo=1
		
		
		
		
		SET @Par_NumErr  			= '000000'
		SET @Par_ErrMenDev			= 'SE HA MODIFICADO LA BASE'
		GOTO ManejoErrores
	END
	
	
	IF @Par_NumAct = @Var_DiaHabil
	BEGIN
	
		
		SET @Par_SucursalID		=  ISNULL(@Par_SucursalID,@CADENA_VACIA)		
		IF @Par_SucursalID			= @CADENA_VACIA	
		BEGIN
			SET	@Par_NumErr		= '000002'
			SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA LA SUCURSAL'
			GOTO ManejoErrores	
		END
	
		SET @Par_MesAnio		=  ISNULL(@Par_MesAnio,@CADENA_VACIA)		
		IF @Par_MesAnio			= @CADENA_VACIA	
		BEGIN
			SET	@Par_NumErr		= '000003'
			SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA LA FECHA'
			GOTO ManejoErrores	
		END
		
	
		
		--ANO
		SELECT @Var_Anio= CAST(RIGHT(RTRIM(@Par_MesAnio),4)AS INT)
		
		--MES
		SELECT @Var_NomMes= (select LEFT(@Par_MesAnio, LEN(@Par_MesAnio) - 5))
		
		SELECT @Var_NomMes =(CASE  @Var_NomMes
		When 'Enero' Then '1'
		When 'Febrero' Then '2'
		When 'Marzo' Then '3'
		When 'Abril' Then '4'
		When 'Mayo' Then '5'
		When 'Junio' Then '6'
		When 'Julio' Then '7'
		When 'Agosto' Then '8'
		When 'Septiembre' Then '9'
		When 'Octubre' Then '10'
		When 'Noviembre' Then '11'
		When 'Diciembre' Then '12'
		END)
		
		SELECT @Var_Mes = cast(@Var_NomMes as int) 
		
		
				
		DELETE DIASHABILES 
			WHERE Mes =@Var_Mes
			AND A�o= @Var_Anio
			AND SucursalID= @Par_SucursalID
		
		--SELECT SucursalID, A�o, Mes
		--FROM DIASHABILES
		--where Mes = 1
		--and A�o= 2021
		--and SucursalID= 'Centro 69'
		--GROUP BY SucursalID, A�o, Mes
		--HAVING COUNT(FechaInhabil) =5;
		
		
		SET @Par_NumErr  			= '000000'
		SET @Par_ErrMenDev			= 'SE HA ELIMINADO EL DIA HABIL'
		GOTO ManejoErrores
	END
	
	
ManejoErrores:	
	
	IF @Par_Salida	= @SalidaSI
	BEGIN	
		SELECT	@Par_NumErr 		AS NumErr,
				@Par_ErrMenDev		AS ErrMenDev
	END

END
