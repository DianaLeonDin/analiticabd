IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[DBO].[BASEALT]') AND type IN ('P'))
	DROP PROCEDURE BASEALT
GO

CREATE PROCEDURE BASEALT(
	------------------------------------------------------------------------------------------------------------------
	-- @nombre:			BASEALT
	-- @autor:			DINERO INMEDIATO
	-- @fecha:			17/12/2019
	-- @version:		1.0
	-- @descripcion:	Store procedure para dar de alta una nueva base de la matriz de indicadores de gestiion de operaciones(catalogo MANTENIMIENTO)
	------------------------------------------------------------------------------------------------------------------|
	
	@Par_Nombre					CHAR(200),
	@Par_Valor					DECIMAL(9,2),--INT,
	--@Par_Frecuencia			CHAR(200),
	@Par_SucursalID				CHAR(200),
	@Par_AliadoID				CHAR(300),
	@Par_MesAnio				CHAR(200),
	@Par_Salida					CHAR(1),				-- Parametro para salida de datos
	@Par_NumErr					CHAR(6) OUT,			-- Parametro de entrada/salida de numero de error
	@Par_ErrMenDev 				VARCHAR(800) OUT		-- Parametro de entrada/salida de mensaje de control de respuesta 
	--@Par_Consecutivo			INT	OUT				    -- Parametro de entrada/salida de para indicar el id que se a generado o actualizado
	
   
		
)
AS

BEGIN

-- Declaracion de constantes 
	DECLARE @ENTERO_VACIO		INT,				-- Entero vacio
			@CADENA_VACIA		CHAR(1),			-- Cadena vacia
			@DECIMAL_VACIO		DECIMAL(9,2),			-- Cadena vacia
			@FECHA_VACIA		DATETIME,			-- Fecha vacia
			@SalidaSI			CHAR(1),			-- Salida si
			@SalidaNO			CHAR(1),			-- Salida no
			@CodigoExito		CHAR(6), 			-- Codigo exito			
			@Var_FolioID		INT
	
		-- Declaracion de variables 
	DECLARE	@Var_TablaID		INT,					-- FOLIO ID
			@Var_BaseReg		INT,					-- Se verifica si el cliente ya tiene una base
			@Var_MontoBase		DECIMAL(14,2),
			@Var_Mes			INT,
			@Var_Anio			INT
		
   
   
-- Asignacion de constantes 
	SELECT	@ENTERO_VACIO		=	0,				-- Asignacion de entero vacio
			@CADENA_VACIA		=	'Todos',				-- Asignacion de cadena vacia
			@DECIMAL_VACIO		=	0000000.00,		-- Asinacion de decimal vacio
			@FECHA_VACIA		=	'1900-01-01',	-- Asignacion de fecha vacia
			@SalidaSI			=	'S',			-- Asignacion de salida si
			@SalidaNO			=	'N',			-- Asignacion de salida no
			@CodigoExito		=	'000000'		-- Codigo exito

	
	SET	@Par_Nombre					        = ISNULL(@Par_Nombre			    ,@CADENA_VACIA)	
	SET	@Par_Valor							= ISNULL(@Par_Valor					,@DECIMAL_VACIO)
	--SET	@Par_Valor						= ISNULL(@Par_Valor					,@ENTERO_VACIO)
	--SET	@Par_Frecuencia					= ISNULL(@Par_Frecuencia			,@CADENA_VACIA)	
	SET	@Par_SucursalID						= ISNULL(@Par_SucursalID			,@CADENA_VACIA)		
	SET	@Par_AliadoID						= ISNULL(@Par_AliadoID				,@CADENA_VACIA)		
	SET	@Par_MesAnio						= ISNULL(@Par_MesAnio				,@CADENA_VACIA)		
	

	IF @Par_Nombre =   @CADENA_VACIA	
	BEGIN
		SET	@Par_NumErr		= '000001'
		SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA EL NOMBRE DE LA BASE'
		GOTO ManejoErrores	
	END
	
	--IF @Par_Valor    =   @DECIMAL_VACIO	
	IF @Par_Valor    =   @ENTERO_VACIO	
	BEGIN
		SET	@Par_NumErr		= '000002'
		SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA EL VALOR DE LA BASE'
		GOTO ManejoErrores	
	END
		
	IF @Par_SucursalID    =   @CADENA_VACIA	
	BEGIN
		SET	@Par_NumErr		= '000003'
		SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA LA SUCURSAL'
		GOTO ManejoErrores	
	END
	
	--	IF @Par_AliadoID    =   @CADENA_VACIA	
	--BEGIN
	--	SET	@Par_NumErr		= '000004'
	--	SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA EL ALIADO'
	--	GOTO ManejoErrores	
	--END
	
		IF @Par_MesAnio    =   @CADENA_VACIA	
	BEGIN
		SET	@Par_NumErr		= '000005'
		SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA EL MES / A�O'
		GOTO ManejoErrores	
	END
	
	SELECT @Var_Anio=(SUBSTRING(@Par_MesAnio, 1, 4) )
	SELECT @Var_Mes= (SUBSTRING(@Par_MesAnio, 6, 2) )
			
    
	--SELECT * FROM BASE				
	INSERT INTO BASE(NomIndicador,			ValBase,			SucursalID,			AliadoID,			Mes,			anio			,Activo ) 

             VALUES (@Par_Nombre,			@Par_Valor,			@Par_SucursalID,	@Par_AliadoID,		@Var_Mes,		@Var_Anio,		1)
	
	SET @Par_NumErr  	= '000000'
	SET @Par_ErrMenDev	= 'SE HA CREADO LA BASE CON �XITO'
	
	
	ManejoErrores:
			
	IF @Par_Salida	= @SalidaSI
	BEGIN

	
		SELECT	@Par_NumErr 		AS NumErr,
				@Par_ErrMenDev		AS ErrMenDev
				--@Par_Consecutivo	AS Consecutivo
	END
	

END
