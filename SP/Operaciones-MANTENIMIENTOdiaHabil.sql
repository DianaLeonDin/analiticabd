IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[DBO].[DIAHABILALT]') AND type IN ('P'))
	DROP PROCEDURE DIAHABILALT
GO

CREATE PROCEDURE DIAHABILALT(
	------------------------------------------------------------------------------------------------------------------
	-- @nombre:			DIAHABILALT
	-- @autor:			DINERO INMEDIATO
	-- @fecha:			18/01/2021
	-- @version:		1.0
	-- @descripcion:	Store procedure para dar de alta un nuevo dia habil del mes de la matriz de indicadores de gestiion de operaciones(catalogo MANTENIMIENTO)
	------------------------------------------------------------------------------------------------------------------|
	
	@Par_SucursalID				CHAR(200),
	@Par_Sucursal				CHAR(300),
	@Par_MesAnio				VARCHAR(200),
	@Par_FechaInhabil			VARCHAR(MAX),
	
	@Par_Salida					CHAR(1),				-- Parametro para salida de datos
	@Par_NumErr					CHAR(6) OUT,			-- Parametro de entrada/salida de numero de error
	@Par_ErrMenDev 				VARCHAR(800) OUT		-- Parametro de entrada/salida de mensaje de control de respuesta 
	--@Par_Consecutivo			INT	OUT				    -- Parametro de entrada/salida de para indicar el id que se a generado o actualizado
)
AS

BEGIN

-- Declaracion de constantes 
	DECLARE @ENTERO_VACIO		INT,				-- Entero vacio
			@CADENA_VACIA		CHAR(1),			-- Cadena vacia
			@DECIMAL_VACIO		DECIMAL(9,2),			-- Cadena vacia
			@FECHA_VACIA		DATETIME,			-- Fecha vacia
			@SalidaSI			CHAR(1),			-- Salida si
			@SalidaNO			CHAR(1),			-- Salida no
			@CodigoExito		CHAR(6), 			-- Codigo exito			
			@Var_FolioID		INT
	
-- Declaracion de variables 
	DECLARE	@Var_TablaID		INT,					-- FOLIO ID
			@Var_BaseReg		INT,					-- Se verifica si el cliente ya tiene una base
			@Var_MontoBase		DECIMAL(14,2),
			@Var_Mes			INT,
			@Var_Anio			INT,
			@Var_FechaInh		DATETIME
		
-- Asignacion de constantes 
	SELECT	@ENTERO_VACIO		=	0,				-- Asignacion de entero vacio
			@CADENA_VACIA		=	'Todos',				-- Asignacion de cadena vacia
			@DECIMAL_VACIO		=	0000000.00,		-- Asinacion de decimal vacio
			@FECHA_VACIA		=	'1900-01-01',	-- Asignacion de fecha vacia
			@SalidaSI			=	'S',			-- Asignacion de salida si
			@SalidaNO			=	'N',			-- Asignacion de salida no
			@CodigoExito		=	'000000'		-- Codigo exito

	SET	@Par_SucursalID					    = ISNULL(@Par_SucursalID		,@CADENA_VACIA)
	SET	@Par_Sucursal					    = ISNULL(@Par_Sucursal			,@CADENA_VACIA)	
	SET	@Par_MesAnio						= ISNULL(@Par_MesAnio			,@CADENA_VACIA)
	SET	@Par_FechaInhabil					= ISNULL(@Par_FechaInhabil		,@CADENA_VACIA)			
	

	IF @Par_SucursalID =   @CADENA_VACIA	
	BEGIN
		SET	@Par_NumErr		= '000001'
		SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA LA SUCURSAL'
		GOTO ManejoErrores	
	END
	
	IF @Par_Sucursal =   @CADENA_VACIA	
	BEGIN
		SET	@Par_NumErr		= '000002'
		SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA LA SUCURSAL'
		GOTO ManejoErrores	
	END
			
	IF @Par_MesAnio    =   @CADENA_VACIA	
	BEGIN
		SET	@Par_NumErr		= '000003'
		SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA EL VALOR DE LA FECHA'
		GOTO ManejoErrores	
	END
		
	IF @Par_FechaInhabil    =   @CADENA_VACIA	
	BEGIN
		SET	@Par_NumErr		= '000004'
		SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA LA FECHA INHABIL'
		GOTO ManejoErrores	
	END
	
	--SELECT @Var_Anio=CAST(SUBSTRING(@Par_MesAnio, 1, 4) AS INT ) 
	--SELECT @Var_Mes= CAST(SUBSTRING(@Par_MesAnio, 6, 2)AS INT)
	SELECT @Var_Anio=SUBSTRING(@Par_MesAnio, 1, 4)
	SELECT @Var_Mes= SUBSTRING(@Par_MesAnio, 6, 2)		
	--CONVERTIR FECHA EN DATETIME 
	--SELECT @Var_FechaInh= CONVERT (datetime,(@Par_FechaInhabil+ '1900-01-01'),103)
	--SELECT CONVERT (datetime,'03-01-2021'+' '+'12:00:00' ,103)
	SELECT @Var_FechaInh=(CONVERT (datetime,@Par_FechaInhabil+' '+'12:00:00' ,103))
	
    			
    			
	INSERT INTO DIASHABILES(Mes,			A�o,			FechaInhabil,			SucursalID,		Sucursal,			Activo) 
             VALUES (@Var_Mes,				@Var_Anio,		@Var_FechaInh,		@Par_SucursalID,	@Par_Sucursal,		1)
	
	SET @Par_NumErr  	= '000000'
	SET @Par_ErrMenDev	= 'SE HA CREADO DIA HABIL CON �XITO'
	
	
	ManejoErrores:
			
	IF @Par_Salida	= @SalidaSI
	BEGIN

	
		SELECT	@Par_NumErr 		AS NumErr,
				@Par_ErrMenDev		AS ErrMenDev
	END
	

END
