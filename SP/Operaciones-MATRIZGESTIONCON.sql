IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[DBO].[MATRIZGESTIONCON]') AND type IN ('P'))
	DROP PROCEDURE MATRIZGESTIONCON
GO

CREATE PROCEDURE MATRIZGESTIONCON(
	------------------------------------------------------------------------------------------------------------------
	-- @nombre:			MATRIZGESTIONCON
	-- @autor:			DINERO INMEDIATO
	-- @fecha:			15/12/2020
	-- @version:		1.0
	-- @descripcion:	Store procedure para consultar el porcentaje de meta de cartera
	------------------------------------------------------------------------------------------------------------------|
	@Par_NumCon			INT,							-- Numero de consulta
	@Par_Sucursal		VARCHAR(200),							--Sucusal
	
	@Par_Anio			INT,							--Filtrar por a�o
	@Par_Mes			INT								--filtrar por mes
)
AS
BEGIN	

		-- Declaracion de constantes 
	DECLARE @ENTERO_VACIO		INT,				-- Entero vacio
			@DECIMAL_VACIO		DECIMAL(8,2),		-- Decimal vacio
			@CADENA_VACIA		CHAR(1),			-- Cadena vacia
			@FECHA_VACIA		DATETIME,			-- Fecha vacia
			@SalidaSI			CHAR(1),			-- Salida si
			@SalidaNO			CHAR(1),			-- Salida no
			@CodigoExito		CHAR(6),			-- Codigo exito
			@Con_MetaCartera	INT,				-- Capita prestado
			@Con_MetaIngreso	INT,
			@Con_ClienteNvo		INT,
			@Con_PromMontoEmp	INT,				--Promedio monto empe�os	
			@Con_PorcMaxEna		INT,				--Porcentaje maximo enajenacion
			@Con_DiasHabil		INT,				--Dias habiles 
			@Con_IngTotal		INT,				--Total de ingresos(Joyeria + Apartados + celulares)
			@Con_CarTotal		INT,				--Cartera total
			@Con_CarTotalMA		INT,
			@Con_ClienteNvoVta	INT,
			@Con_ClienteNvoVtaMA INT, 
			--POR ALIDO
			@Con_PPAliado		INT,	
			@Con_MCAliado		INT,
			@Con_CNuevoAliado	INT,
			@Con_IEAliado		INT,
			--POR ALIDO MES ANTERIOR
			@Con_PPAliadoMA		INT,	
			@Con_MCAliadoMA		INT,
			@Con_CNuevoAliadoMA	INT,
			@Con_IEAliadoMA		INT,
			--POR MES ANTERIOR
			@Con_PMEmpenioMes	INT,					--Promedio monto empe�os, del mes anterior 
			@Con_PMEnajMEs		INT,					--Porcentaje maximo enajenacion por mes anterior
			@Con_MCartMes		INT,					--Meta cartera del mes anterior
			@Con_CNuevoMes		INT,
			@Con_MIngresoMes	INT,
			--POR PRODUCTO
			@Con_PPProducto		INT,	
			@Con_MCProducto		INT,
			@Con_CNuevoProducto	INT,
			@Con_MIngProducto	INT,	
			@Con_PPProductoMA	INT,					-- mes anterior promedio preestamo
			@Con_CPProductoMA	INT,					----mes anterior capital prestado
			@Con_CNProductoMA	INT,
			@Con_MIngProductoMA	INT,
			--ANUAL
			@Con_ApresProm		INT,
			@Con_APorcMaxE		INT,
			@Con_ACapPres		INT,
			@Con_ACteNvo		INT,
			@Con_ACteNvoVta		INT,
			@Con_ACartTotal		INT,
			@Con_AIngresoEmp	INT
			
	-- Declaracion de variables 
	DECLARE	@Var_MenPersUno		VARCHAR(100),		--  Mensaje de personalizado para la tabla de MENSAJESISTEMA para el campo MensajeDev
			@Var_EstatusActual	INT,				-- AlMacenar el estatus actual del estatus
			@Var_ExisteCred		INT,				-- SERVIRA PARA VALIDAR SI EL ESTATUS EXISTE
			@Var_FechaActual	DATETIME,
			@Var_DosMeses		DATETIME
			
	-- Asignacion de constantes 
	SELECT	@ENTERO_VACIO		=	0,				-- Asignacion de entero vacio
			@DECIMAL_VACIO		=	0.0,
			@CADENA_VACIA		=	'',				-- Asignacion de cadena vacia
			@FECHA_VACIA		=	'1900-01-01',	-- Asignacion de fecha vacia
			@SalidaSI			=	'S',			-- Asignacion de salida si
			@SalidaNO			=	'N',			-- Asignacion de salida no
			@CodigoExito		=	'000000',		-- Codigo exito
			@Con_MetaCartera	=	1,				--Capital prestado
			@Con_MetaIngreso	=	2,				--Meta ingreso
			@Con_ClienteNvo		=	3,				--Cliente nuevo
			@Con_PromMontoEmp	=	4,
			@Con_PorcMaxEna		=	5,
			@Con_DiasHabil		=	6,
			@Con_IngTotal		=	7,
			@Con_CarTotal		=	8,
			@Con_CarTotalMA		=	9,
			@Con_ClienteNvoVta	=	50,
			@Con_ClienteNvoVtaMA=	51,
			--POR ALIDO
			@Con_PPAliado		=	20,
			@Con_MCAliado		=	21,
			@Con_CNuevoAliado	=	22,
			@Con_PPAliadoMA		=	23,	
			@Con_MCAliadoMA		=	24,
			@Con_CNuevoAliadoMA	=	25,
			@Con_IEAliado		=	26,
			@Con_IEAliadoMA		=	27,
			--POR MES ANTERIOR
			@Con_PMEmpenioMes	=	10,
			@Con_PMEnajMEs		=	11,
			@Con_MCartMes		=	12,
			@Con_CNuevoMes		=	13,
			@Con_MIngresoMes	=	14,
			--POR PRODUCTO
			@Con_PPProducto		=	30,	
			@Con_MCProducto		=	31,
			@Con_CNuevoProducto	=	32,
			@Con_MIngProducto	=	33, --Meta ingreso(Empe�o)
			
			@Con_PPProductoMA   =	40,
			@Con_CPProductoMA	=	41,
			@Con_CNProductoMA	=	42,
			@Con_MIngProductoMA	=	43,
			--ANUAL
			@Con_ApresProm		=	60,
			@Con_APorcMaxE		=	61,
			@Con_ACapPres		=	62,
			@Con_ACteNvo		=	63,
			@Con_ACteNvoVta		=	64,
			@Con_ACartTotal		=	65,
			@Con_AIngresoEmp	=	66

	
		---------------	 *** INDICADOR DE MATRIZ DE INDICADORES DE GESTION(DIARIO)
		
		--REAL del Capital Prestado
		IF	@Par_numCon =	@Con_MetaCartera
		BEGIN

			--SELECT sum(capital) as Cartera, 'Todos' as Producto
				--FROM [Empe�o].[dbo].[vwEmpMovttoCartera] where AnnEmp = YEAR(GETDATE()) and MesEmp=MONTH(GETDATE()) and Status<>'Cancelada'
				
				SELECT ISNULL(sum(capital), 0) as Cartera, 'Todos' as Producto
					FROM [Empe�o].[dbo].[vwEmpMovttoCartera] 
					where convert(datetime,cast(DiaEmp as varchar(200)) +'/'+ cast(MesEmp as varchar(200)) +'/'+cast(annEmp as varchar(200)), 103)
					<=(SELECT DATEADD(d,-1,GETDATE()))
					and AnnEmp = YEAR(GETDATE()) and MesEmp=MONTH(GETDATE()) and Status<>'Cancelada'
							
		END
		
		--REAL de % Meta Ingresos
		--Ingreso (empe�o)
		IF	@Par_numCon =	@Con_MetaIngreso
		BEGIN

			--SELECT SUM(intereses) AS intereses, SUM(recargos) AS recargos, SUM(reimCosto) AS reimCosto,
			--	SUM(intereses)+ SUM(recargos)+ SUM(reimCosto) AS total  FROM 
			--	dbo.vwIndMovPagos
			--	WHERE annMov= YEAR(getdate())and MesMov= MONTH(getdate())
			
							
			--SELECT  SUM(Interes + Recargos + PagReimp) as Ingreso
			--	FROM     empe�o.dbo.vwEmpMovtosResumenPagos
			--	WHERE  (AnnPag = YEAR(getdate())) AND (MesPag = MONTH(getdate()))
			
			--SELECT ISNULL(SUM(Interes + Recargos + PagReimp),0) as Ingreso
			--	FROM     empe�o.dbo.vwEmpMovtosResumenPagos				
			--	WHERE  (AnnPag = YEAR(getdate())) AND (MesPag = MONTH(getdate()))
			--	and convert(datetime,cast(DiaPag as varchar(200)) +'/'+ cast(MesPag as varchar(200)) +'/'+cast(AnnPag as varchar(200)), 103)
			--	<=(SELECT DATEADD(d,-1,GETDATE()))
				
			SELECT  SUM(Interes + Recargos + PagReimp) as Ingreso
				FROM     empe�o.dbo.vwEmpMovtosResumenPagos
				WHERE  (AnnPag = YEAR(getdate())) 
				AND (MesPag = MONTH(getdate())) and DiaPag < DAY(GETDATE())
				
				
		END
		
		--REAL Clientes nuevos empe�os
		IF	@Par_numCon =	@Con_ClienteNvo
		BEGIN
		
			--SELECT ISNULL(SUM(NumCli),@ENTERO_VACIO) AS TotalCteNvo
			--	FROM dbo.vwIndClientesNuevos
			--	WHERE AnnEmp=YEAR(GETDATE()) 
			--	AND MesEmp= MONTH(GETDATE())
			
			--SELECT ISNULL(SUM(NumCli),@ENTERO_VACIO) AS TotalCteNvo
			--	FROM dbo.vwIndClientesNuevos
			--	WHERE AnnEmp=YEAR(GETDATE()) 
			--	AND MesEmp= MONTH(GETDATE())
			--	AND convert(datetime,cast(DiaEmp as varchar(200)) +'/'+ cast(MesEmp as varchar(200)) +'/'+cast(annEmp as varchar(200)), 103)
			--	<=(SELECT DATEADD(d,-1,GETDATE()))
			
			SELECT  ISNULL(SUM(Capital) / COUNT(*), 0.00) AS TotalCteNvo, COUNT(distinct CveCli) as NumClietes
				 --AS Promedio, COUNT(distinct CveCli) as NumClietes
				FROM     vwEmpMovttoGral
				WHERE  (CveCli IN
								   (SELECT  CVECLI
									FROM     ListaClientesNuevos AS Lista
									WHERE  (YEAR(FECALT) = YEAR(GETDATE())) AND (MONTH(FECALT) = MONTH(GETDATE()))
									AND (vwEmpMovttoGral.AnnEmp = year(GETDATE())) AND (vwEmpMovttoGral.MesEmp = month(GETDATE()))))
			
		END
		
		--REAL Promedio monto empe�o (Prestamo Promedio)
		IF	@Par_numCon =	@Con_PromMontoEmp
		BEGIN
			  SELECT @Var_FechaActual =(DATEADD(d,-1,GETDATE()))
			 
			  SELECT ISNULL(SUM(Capital),0) AS Capital, ISNULL(COUNT(FolOri),0) AS Folios, ISNULL(SUM(Capital)/ COUNT(FolOri),0) AS PromedioSem
				FROM dbo.vwEmpMovttoGral
				WHERE  FecIni <=(SELECT DATEADD(d,-1,GETDATE()))
				AND AnnEmp = YEAR(GETDATE()) 
				and MesEmp=MONTH(GETDATE())
				AND Status<>'Cancelada'	 
								
			 -- SELECT SUM(Capital) AS Capital, COUNT(FolOri) AS Folios, SUM(Capital)/ COUNT(FolOri) AS PromedioSem
				--FROM dbo.vwEmpMovttoGral
				--WHERE  YEAR(FecIni)=year(getdate()) 
				--AND MONTH(FecIni)=MONTH(GETDATE())
				--AND DAY(FecIni)< DAY(DATEADD(d,-1,GETDATE()))
				----AND CveSuc=1006  
				--AND Status<>'Cancelada'				
		END
		
		--REAL Porcentaje Maximo de enajenacion
		IF	@Par_numCon =	@Con_PorcMaxEna
		BEGIN
		
				--SELECT COUNT(DISTINCT movtos.folmov) Folios
				--	FROM     Movtos 
				--	WHERE  YEAR(Movtos.FecEnajenado)=YEAR(GETDATE()) 
				--	AND month(Movtos.FecEnajenado)=month(GETDATE())  AND (Movtos.Enajenado = 1) 
				--	AND (Movtos.FolMov = dbo.GetFolioUlt(Movtos.FolMov, Movtos.CveSuc)) AND LoteEnaj <> ''
				
				
				--SELECT top 1 Folios FROM dbo.vwIndEnajenados3
				--	where 	AnnMov=YEAR(GETDATE()) and MesMov=MONTH(GETDATE())
				--	order by DiaMov desc
				
				SELECT TOP 1  [PorcEnaj] AS Folios, diamov
				  FROM [empe�o].[dbo].[vwIndEnajenados3] WHERE [AnnMov]=YEAR(GETDATE())
				  AND [MesMov] =MONTH(GETDATE()) order by diamov desc

		
		END
		
		--Real Total de ingreso (total de joyeria, apartad y celular)
		IF	@Par_numCon =	@Con_IngTotal
		BEGIN
				--SELECT SUM(ImpTot) + (SELECT  SUM(ImpPag) AS Expr1
				--						FROM     vwIndIngresoApartados
				--						WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod) ) as IngTotal                    
				--FROM     vwIndIngresoVentas AS a
				--WHERE  (AnnMov = YEAR(GETDATE())) AND (MesMov = MONTH(GETDATE())) 
				--AND (CveBod = @Par_Sucursal) AND (CveMov IN (8, 37))
				--GROUP BY AnnMov, MesMov, CveBod
				
				SELECT SUM(ImpTot) + (SELECT  SUM(ImpPag) AS Expr1
										FROM     vwIndIngresoApartados
										WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod) ) as IngTotal                    
				FROM     vwIndIngresoVentas AS a
				WHERE  FecMov <= (SELECT DATEADD(d,-1,GETDATE()))and
				(AnnMov = YEAR(GETDATE())) AND (MesMov = MONTH(GETDATE())) 
				AND (CveBod = 6) AND (CveMov IN (8, 37))
				GROUP BY AnnMov, MesMov, CveBod
				
		END
		
		--Real CARTERA TOTAL
		IF	@Par_numCon =	@Con_CarTotal
		BEGIN
		
			SELECT @Var_FechaActual = (SELECT DATEADD(dd, DATEPART(dd, GETDATE())* -1,  GETDATE()))

			SELECT SUM(CapitalVigente)  AS CarteraTotal FROM [vwIndCartera] 
							WHERE AnnCar=YEAR(@Var_FechaActual) AND MesCar=MONTH(@Var_FechaActual) AND DiaCar=DAY(@Var_FechaActual)
							
			--IF ((SELECT COUNT(*) from [vwIndCartera] where AnnCar=YEAR(GETDATE()) and MesCar=month(GETDATE()) and DiaCar=day(GETDATE()))=0) --Hay resultados
			--BEGIN
			--		SELECT @Var_FechaActual = (SELECT CONVERT(DATETIME,CONVERT(VARCHAR(10), GETDATE(), 112)+ ' ' + '23:59:59.00') AS FECHACTUAL )
				
			--		EXECUTE sp_getcortecajaplazos3 @Var_FechaActual,99
				
			--		SELECT 
			--			( SELECT SUM(CapitalVigente) actual FROM [vwIndCartera] 
			--				WHERE AnnCar=YEAR(GETDATE()) AND MesCar=MONTH(GETDATE()) AND DiaCar=DAY(GETDATE())
			--			)
			--			-
			--			( SELECT SUM(CapitalVigente) FROM ( SELECT TOP 1(CapitalVigente) 
			--			FROM [vwIndCartera] WHERE AnnCar=YEAR(GETDATE()) and MesCar=MONTH(GETDATE()) AND Tipo='Aparatos'
			--			UNION
			--			SELECT TOP 1(CapitalVigente) 
			--			FROM [vwIndCartera] WHERE AnnCar=YEAR(GETDATE()) AND MesCar=MONTH(GETDATE()) AND Tipo='Celulares'
			--			UNION
			--			SELECT top 1(CapitalVigente) 
			--			FROM [vwIndCartera] WHERE AnnCar=YEAR(GETDATE()) AND MesCar=MONTH(GETDATE()) AND Tipo='Joyeria') a 
			--			)  AS CarteraTotal					
			--END
			--ELSE
			--BEGIN
			--		SELECT 
			--			( SELECT SUM(CapitalVigente) actual FROM [vwIndCartera] 
			--				WHERE AnnCar=YEAR(GETDATE()) AND MesCar=MONTH(GETDATE()) AND DiaCar=DAY(GETDATE())
			--			)
			--			-
			--			( SELECT SUM(CapitalVigente) FROM ( SELECT TOP 1(CapitalVigente) 
			--			FROM [vwIndCartera] WHERE AnnCar=YEAR(GETDATE()) and MesCar=MONTH(GETDATE()) AND Tipo='Aparatos'
			--			UNION
			--			SELECT TOP 1(CapitalVigente) 
			--			FROM [vwIndCartera] WHERE AnnCar=YEAR(GETDATE()) AND MesCar=MONTH(GETDATE()) AND Tipo='Celulares'
			--			UNION
			--			SELECT top 1(CapitalVigente) 
			--			FROM [vwIndCartera] WHERE AnnCar=YEAR(GETDATE()) AND MesCar=MONTH(GETDATE()) AND Tipo='Joyeria') a 
			--			)  AS CarteraTotal
			--END
		END
		
		--REAL Clientes nuevos ventas
		IF	@Par_numCon =	@Con_ClienteNvoVta
		BEGIN
		
			SELECT  COUNT(CATCLI.CVECLI) AS NumClientes
				FROM     CATCLI INNER JOIN
							   MOVTOS ON CATCLI.CVECLI = MOVTOS.CveProvCli
				WHERE  (YEAR(CATCLI.FECALT) = YEAR(GETDATE())) AND (MONTH(CATCLI.FECALT) = MONTH(GETDATE()))
				GROUP BY MOVTOS.CveBod
				HAVING  (MOVTOS.CveBod = @Par_Sucursal)
			
		END
		
		---------------	 *** INDICADOR DE MATRIZ DE INDICADORES DE GESTION(DIARIO DEL MES ANTERIOR)
		
		--REAL Promedio monto empe�o (Prestamo Promedio)
		IF	@Par_numCon =	@Con_PMEmpenioMes
		BEGIN
				
			  SELECT SUM(Capital) AS Capital, COUNT(FolOri) AS Folios, SUM(Capital)/ COUNT(FolOri) AS PromedioSem
				FROM dbo.vwEmpMovttoGral
				WHERE  YEAR(FecIni)=@Par_Anio 
				AND MONTH(FecIni)=@Par_Mes
				--AND CveSuc=1006  
				AND Status<>'Cancelada'		
					
		END
		
		--REAL Porcentaje Maximo de enajenacion
		IF	@Par_numCon =	@Con_PMEnajMEs
		BEGIN
		
				SELECT  PorcEnaj AS Folios FROM dbo.vwIndEnajenados3
				WHERE AnnMov =@Par_Anio AND MesMov = @Par_Mes

		END
		
		--REAL del Capital Prestado(Meta cartera)
		IF	@Par_numCon =	@Con_MCartMes
		BEGIN
		
			SELECT @Var_FechaActual =(DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))

			SELECT sum(capital) as Cartera, 'Todos' as Producto
				FROM [Empe�o].[dbo].[vwEmpMovttoCartera] 
				WHERE AnnEmp = YEAR(@Var_FechaActual) AND MesEmp=MONTH(@Var_FechaActual)
				AND Status<>'Cancelada'
				
				
		END
		
		--REAL Clientes nuevos empe�os
		IF	@Par_numCon =	@Con_CNuevoMes
		BEGIN
		
			--SELECT SUM(NumCli) AS TotalCteNvo
			--	FROM dbo.vwIndClientesNuevos
			--	WHERE AnnEmp=@Par_Anio 
			--	AND MesEmp= @Par_Mes
			
			SELECT  ISNULL(SUM(Capital) / COUNT(*), 0.00) AS TotalCteNvo, COUNT(distinct CveCli) as NumClietes
				-- AS Promedio, COUNT(distinct CveCli) as NumClietes
				FROM     vwEmpMovttoGral
				WHERE  (CveCli IN
								   (SELECT  CVECLI
									FROM     ListaClientesNuevos AS Lista
									WHERE  (YEAR(FECALT) = @Par_Anio) AND (MONTH(FECALT) = @Par_Mes)
									AND (vwEmpMovttoGral.AnnEmp = @Par_Anio) AND (vwEmpMovttoGral.MesEmp = @Par_Mes)))
			
		END
		
		--REAL de % Meta Ingresos
		IF	@Par_numCon =	@Con_MIngresoMes
		BEGIN

			SELECT  SUM(Interes + Recargos + PagReimp) as Ingreso
				FROM     empe�o.dbo.vwEmpMovtosResumenPagos
				WHERE  (AnnPag = @Par_Anio) 
				AND (MesPag = @Par_Mes)
				
		END
		
		--Real CARTERA TOTAL
		IF	@Par_numCon =	@Con_CarTotalMA
		BEGIN
			--1 Mes anterior 
			--SELECT @Var_FechaActual =(DATEADD(dd, DATEPART(dd, GETDATE())* -1,  GETDATE()))
			--2 Mes anterior , por medio del mes anterior 1
			--SELECT @Var_DosMeses = DATEADD(dd, DATEPART(dd, @Var_FechaActual)* -1,  @Var_FechaActual)
			
			select @Var_FechaActual= (select DATEADD(dd, DATEPART(dd, DATEADD(dd, DATEPART(dd, GETDATE())* -1,  GETDATE()))* -1,  DATEADD(dd, DATEPART(dd, GETDATE())* -1,  GETDATE())))
			
			
			
			SELECT SUM(CapitalVigente)  AS CarteraTotal FROM [vwIndCartera] 
							WHERE AnnCar=YEAR(@Var_FechaActual) AND MesCar=MONTH(@Var_FechaActual) AND DiaCar=DAY(@Var_FechaActual)
							
			--EXECUTE sp_getcortecajaplazos3 @Var_FechaActual,99	
	
			--		SELECT 
			--			( 
			--				select SUM(CapitalVigente) as CarteraTotal from( 
			--					SELECT CapitalVigente, DiaCar
			--					FROM (
			--						SELECT TOP 1 CapitalVigente, DiaCar FROM [vwIndCartera]
			--						WHERE AnnCar=YEAR(@Var_FechaActual) AND MesCar=MONTH(@Var_FechaActual) AND Tipo='Aparatos'
			--						order by DiaCar asc								    
			--						UNION ALL								    
			--						SELECT TOP 1 CapitalVigente, DiaCar FROM [vwIndCartera]
			--						WHERE AnnCar=YEAR(@Var_FechaActual) AND MesCar=MONTH(@Var_FechaActual) AND Tipo='Celulares'
			--						order by DiaCar asc								    
			--						UNION ALL								    
			--						SELECT TOP 1 CapitalVigente, DiaCar FROM [vwIndCartera]
			--						WHERE AnnCar=YEAR(@Var_FechaActual) AND MesCar=MONTH(@Var_FechaActual) AND Tipo='Joyeria'
			--						order by DiaCar asc
								    
			--					) CTotal 
			--					--GROUP BY DiaCar, CapitalVigente --order by DiaCar asc
			--					)as CTotal
			--			)
			--			-
			--			( 
			--				select SUM(CapitalVigente) as CarteraTotal from( 
			--					SELECT CapitalVigente, DiaCar
			--					FROM (
			--						SELECT TOP 1 CapitalVigente, DiaCar FROM [vwIndCartera]
			--						WHERE AnnCar=YEAR(@Var_FechaActual) AND MesCar=MONTH(@Var_FechaActual) AND Tipo='Aparatos'
			--						order by DiaCar desc								    
			--						UNION ALL								    
			--						SELECT TOP 1 CapitalVigente, DiaCar FROM [vwIndCartera]
			--						WHERE AnnCar=YEAR(@Var_FechaActual) AND MesCar=MONTH(@Var_FechaActual) AND Tipo='Celulares'
			--						order by DiaCar desc								    
			--						UNION ALL
			--					    SELECT TOP 1 CapitalVigente, DiaCar FROM [vwIndCartera]
			--						WHERE AnnCar=YEAR(@Var_FechaActual) AND MesCar=MONTH(@Var_FechaActual) AND Tipo='Joyeria'
			--						order by DiaCar desc								    
			--					) CTotal 
			--					--GROUP BY DiaCar, CapitalVigente --order by DiaCar asc
			--					)as CTotal
			--			)  AS CarteraTotal
			
		END
		
		--REAL Clientes nuevos ventas
		IF	@Par_numCon =	@Con_ClienteNvoVtaMA
		BEGIN
		
			SELECT @Var_FechaActual= (DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
						
			SELECT  COUNT(CATCLI.CVECLI) AS NumClientes
				FROM     CATCLI INNER JOIN
							   MOVTOS ON CATCLI.CVECLI = MOVTOS.CveProvCli
				WHERE  (YEAR(CATCLI.FECALT) = YEAR(@Var_FechaActual)) AND (MONTH(CATCLI.FECALT) = MONTH(@Var_FechaActual))
				GROUP BY MOVTOS.CveBod
				HAVING  (MOVTOS.CveBod = @Par_Sucursal)
			
		END
		---------------	 *** INDICADOR DE MATRIZ DE INDICADORES DE GESTION(POR ALIADO)
				
		--REAL Promedio monto empe�o (Prestamo Promedio por aliado)
		IF	@Par_numCon =	@Con_PPAliado
		BEGIN				
				
			SELECT  
				SUM(Capital) AS Capital, COUNT(FolOri) AS Folios, 
				SUM(Capital) / COUNT(FolOri) AS PromedioSem
				FROM     vwEmpMovttoGral
				WHERE  (YEAR(FecIni) = YEAR(GETDATE())) 
				AND (MONTH(FecIni) = MONTH(GETDATE()))
				AND (Status <> 'Cancelada') and
				login= @Par_Sucursal  
				--CveUserEmp=154
				--CveUserEmp= @Par_Sucursal					
		END
		
		--REAL del Capital Prestado(Meta cartera por aliado)
		IF	@Par_numCon =	@Con_MCAliado
		BEGIN

			 SELECT  SUM(capitalOri) as Cartera
				  FROM [empe�o].[dbo].[vwIndMovttoPrestadoAliado] 
				  where AnnEmp = YEAR(GETDATE()) and MesEmp=MONTH(GETDATE()) and Status<>'Cancelada'
				  and login= @Par_Sucursal  				
		END
		
		--REAL Clientes nuevos
		IF	@Par_numCon =	@Con_CNuevoAliado
		BEGIN
		
			SELECT ISNULL(SUM(NumCli), @ENTERO_VACIO)AS TotalCteNvo
				FROM dbo.vwIndClientesNuevos
				WHERE AnnEmp=YEAR(GETDATE()) 
				AND MesEmp= MONTH(GETDATE()) 
				AND NomUser= @Par_Sucursal  
			
		END
		
		--REAL Meta ingresos (empe�o)---(INGRESO (EMPE�O))
		IF	@Par_numCon =	@Con_IEAliado
		BEGIN
		
			SELECT SUM(vrm.Interes + vrm.Recargos + vrm.PagReimp) AS Ingreso
				--FROM [vwIndResumenPagos] AS vrm 
				FROM vwIndResumenPagos AS vrm
				INNER JOIN Users AS U ON vrm.CveUser = U.CveUser
				WHERE  (vrm.AnnPag = YEAR(GETDATE())) AND (vrm.MesPag = MONTH(GETDATE())) and U.Login=@Par_Sucursal 	
				
						
		END
		
		
		---------------	 *** INDICADOR DE MATRIZ DE INDICADORES DE GESTION(POR ALIADO MES ANTERIOR)
		
		--REAL Promedio monto empe�o (Prestamo Promedio por aliado)
		IF	@Par_numCon =	@Con_PPAliadoMA
		BEGIN				
			
			SELECT @Var_FechaActual =(DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
			
			SELECT  SUM(Capital) AS Capital, COUNT(FolOri) AS Folios, SUM(Capital) / COUNT(FolOri) AS PromedioSem
				FROM     vwEmpMovttoGral
				WHERE  (YEAR(FecIni) = YEAR(@Var_FechaActual))
				AND (MONTH(FecIni) = MONTH(@Var_FechaActual))
				AND (Status <> 'Cancelada') and  
				--CveUserEmp=154
				login= @Par_Sucursal  
								
		END
		
		--REAL del Capital Prestado(Meta cartera por aliado)
		IF	@Par_numCon =	@Con_MCAliadoMA
		BEGIN

			SELECT @Var_FechaActual =(DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
			
			 SELECT  SUM(capitalOri) as Cartera
				  FROM [empe�o].[dbo].[vwIndMovttoPrestadoAliado] 
				  where AnnEmp = YEAR(@Var_FechaActual) 
				  AND MesEmp=MONTH(@Var_FechaActual)
				  AND Status<>'Cancelada'
				  and login= @Par_Sucursal  			
		END
		
		--REAL Clientes nuevos
		IF	@Par_numCon =	@Con_CNuevoAliadoMA
		BEGIN
			
			SELECT @Var_FechaActual =(DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
			
			SELECT ISNULL(SUM(NumCli), 0)AS TotalCteNvo
				FROM dbo.vwIndClientesNuevos
				WHERE AnnEmp=YEAR(@Var_FechaActual) 
				AND MesEmp= MONTH(@Var_FechaActual)
				AND NomUser= @Par_Sucursal  
			
		END
		
		--REAL Meta ingresos (empe�o)---(INGRESO (EMPE�O))
		IF	@Par_numCon =	@Con_IEAliadoMA
		BEGIN
		
			SELECT @Var_FechaActual =(DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
		
			SELECT SUM(vrm.Interes + vrm.Recargos + vrm.PagReimp) AS Ingreso
				FROM vwIndResumenPagos AS vrm 
				INNER JOIN Users AS U ON vrm.CveUser = U.CveUser
				WHERE  (vrm.AnnPag = YEAR(@Var_FechaActual)) AND (vrm.MesPag = MONTH(@Var_FechaActual)) and U.Login=@Par_Sucursal 			
				
				
		END
			
		---------------	 *** INDICADOR DE MATRIZ DE INDICADORES DE GESTION(POR PRODUCTO)
		
		--REAL Promedio monto empe�o (Prestamo Promedio por producto)
		IF	@Par_numCon =	@Con_PPProducto
		BEGIN				
			
			SELECT  ISNULL(SUM(Capital), 0) AS Capital, COUNT(FolOri) AS Folios, ISNULL(SUM(Capital) / COUNT(FolOri),0) AS PromedioSem
				FROM     vwEmpMovttoGral
				WHERE  (YEAR(FecIni) = YEAR(GETDATE())) 
				AND (MONTH(FecIni) = MONTH(GETDATE())) 
				AND (Status <> 'Cancelada') 
				AND Producto=@Par_Sucursal					
		END
		
		--REAL del Capital Prestado(Meta cartera por producto)
		IF	@Par_numCon =	@Con_MCProducto
		BEGIN
		
			SELECT  SUM(capital) AS Cartera
			  FROM [empe�o].[dbo].[vwIndMovttoPrestadoProd] 
			  WHERE AnnEmp = YEAR(GETDATE()) 
			  AND MesEmp=MONTH(GETDATE()) 
			  AND Status<>'Cancelada'
			 -- AND CveTipProd=@Par_Sucursal
			 AND Producto=@Par_Sucursal
			
		END
		
		--REAL Clientes nuevos
		IF	@Par_numCon =	@Con_CNuevoProducto
		BEGIN
		
			SELECT ISNULL(SUM(NumCli), @ENTERO_VACIO)AS TotalCteNvo
				FROM dbo.vwIndClientesNuevos
				WHERE AnnEmp=YEAR(GETDATE()) 
				AND MesEmp= MONTH(GETDATE()) 
				--AND cvetipprod = @Par_Sucursal 
				AND DesTipProd = @Par_Sucursal 
			
		END
				
		--REAL Meta ingresos (empe�o)---(INGRESO (EMPE�O))
		IF	@Par_numCon =	@Con_MIngProducto
		BEGIN
		
			SELECT  ISNULL(SUM(ISNULL(vrm.Interes,0) + ISNULL(vrm.Recargos,0) + ISNULL(vrm.PagReimp,0)),0) AS Ingreso
				FROM     vwIndResumenPagos AS vrm 
				INNER JOIN Users AS U ON vrm.CveUser = U.CveUser
				WHERE  (vrm.AnnPag = YEAR(GETDATE())) AND (vrm.MesPag = MONTH(GETDATE())) AND vrm.Destipprod=@Par_Sucursal 

					
		END
		
		
		---------------	 *** INDICADOR DE MATRIZ DE INDICADORES DE GESTION(POR PRODUCTO MES ANTERIOR)
		
		
		--REAL Promedio monto empe�o (Prestamo Promedio por producto)
		IF	@Par_numCon =	@Con_PPProductoMA
		BEGIN				
			SELECT @Var_FechaActual =(DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
						
			SELECT  ISNULL(SUM(Capital), @ENTERO_VACIO) AS Capital, COUNT(FolOri) AS Folios, ISNULL(SUM(Capital) / COUNT(FolOri),@ENTERO_VACIO) AS PromedioSem
				FROM     vwEmpMovttoGral
				WHERE  YEAR(FecIni) = YEAR(@Var_FechaActual) 
				AND MONTH(FecIni) = MONTH(@Var_FechaActual) 
				AND (Status <> 'Cancelada') 
				AND CveTipProd=@Par_Sucursal					
		END
		
		--REAL del Capital Prestado(Meta cartera por producto)
		IF	@Par_numCon =	@Con_CPProductoMA
		BEGIN
		
			SELECT @Var_FechaActual =(DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
			
			
			SELECT  SUM(capital) AS Cartera
			  FROM [empe�o].[dbo].[vwIndMovttoPrestadoProd] 
			  WHERE AnnEmp = YEAR(@Var_FechaActual)
			  AND MesEmp=MONTH(@Var_FechaActual)
			  AND Status<>'Cancelada'
			  AND CveTipProd=@Par_Sucursal
				
		END
		
		--REAL Clientes nuevos
		IF	@Par_numCon =	@Con_CNProductoMA
		BEGIN
		
			SELECT @Var_FechaActual =(DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
			
			SELECT ISNULL(SUM(NumCli),0)AS TotalCteNvo
				FROM dbo.vwIndClientesNuevos
				WHERE AnnEmp=YEAR(@Var_FechaActual) 
				AND MesEmp= MONTH(@Var_FechaActual)
				AND cvetipprod = @Par_Sucursal 
			
		END
		
		--REAL Meta ingresos (empe�o)---(INGRESO (EMPE�O))
		IF	@Par_numCon =	@Con_MIngProductoMA
		BEGIN
		
			SELECT @Var_FechaActual =(DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
			
			SELECT  ISNULL(SUM(ISNULL(vrm.Interes,0) + ISNULL(vrm.Recargos,0) + ISNULL(vrm.PagReimp,0)),0) AS Ingreso
				FROM     vwIndResumenPagos AS vrm 
				INNER JOIN Users AS U ON vrm.CveUser = U.CveUser
				WHERE  (vrm.AnnPag = YEAR(@Var_FechaActual)) AND (vrm.MesPag = MONTH(@Var_FechaActual)) AND vrm.Destipprod=@Par_Sucursal 

					
		END
		
		---------------	 *** INDICADOR DE MATRIZ DE INDICADORES DE GESTION(POR A�O)
		
		--REAL Prestamo promedio
		IF	@Par_numCon =	@Con_ApresProm
		BEGIN
		
			SELECT COUNT(FolOri) AS Folios, SUM(Capital)/ COUNT(FolOri) AS PromedioSem
				FROM dbo.vwEmpMovttoGral
				WHERE  AnnEmp = @Par_Anio
				AND Status<>'Cancelada'	 
					
		END
		
		--REAL Porcentaje Maximo de enajenacion
		IF	@Par_numCon =	@Con_APorcMaxE
		BEGIN
		
				SELECT TOP 1 Folios FROM dbo.vwIndEnajenados3
					WHERE 	AnnMov= @Par_Anio
					ORDER BY DiaMov DESC
		
		END
		
		--REAL del Capital Prestado
		IF	@Par_numCon =	@Con_ACapPres
		BEGIN
			
				SELECT sum(capital) as Cartera, 'Todos' as Producto
					FROM [Empe�o].[dbo].[vwEmpMovttoCartera] 
					WHERE AnnEmp = @Par_Anio  AND Status<>'Cancelada'
												
		END
		
		--REAL Clientes nuevos empe�os
		IF	@Par_numCon =	@Con_ACteNvo
		BEGIN
		
			SELECT  ISNULL(SUM(Capital) / COUNT(*), 0.00) AS TotalCteNvo, COUNT(distinct CveCli) as NumClietes
				--AS Promedio, COUNT(distinct CveCli) as NumClietes
				FROM     vwEmpMovttoGral
				WHERE  (CveCli IN
				(SELECT  CVECLI
				FROM     ListaClientesNuevos AS Lista
				WHERE  (YEAR(FECALT) = @Par_Anio))
				AND (vwEmpMovttoGral.AnnEmp = @Par_Anio))
		END
			
		--REAL Clientes nuevos ventas
		IF	@Par_numCon =	@Con_ACteNvoVta
		BEGIN
			
			SELECT  COUNT(CATCLI.CVECLI) AS NumClientes
				FROM     CATCLI INNER JOIN
							   MOVTOS ON CATCLI.CVECLI = MOVTOS.CveProvCli
				WHERE  (YEAR(CATCLI.FECALT) = @Par_Anio)
				GROUP BY MOVTOS.CveBod
				HAVING  (MOVTOS.CveBod = @Par_Sucursal)
			
			
		END
		
		--REAL Cartera total
		IF	@Par_numCon =	@Con_ACartTotal
		BEGIN
		
			SELECT SUM(CapitalVigente)  AS CarteraTotal FROM [vwIndCartera] 
							WHERE AnnCar=@Par_Anio 
			
		END
		
		--REAL Ingresos (empe�o)
		IF	@Par_numCon =	@Con_AIngresoEmp
		BEGIN
				SELECT SUM(Interes + Recargos + PagReimp) as Ingreso
				FROM     empe�o.dbo.vwEmpMovtosResumenPagos				
				WHERE  (AnnPag = @Par_Anio)
				
		END
		
		
		
END
