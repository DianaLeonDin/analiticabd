IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[DBO].[MATRIZGESTIONLIS]') AND type IN ('P'))
	DROP PROCEDURE MATRIZGESTIONLIS
GO

CREATE PROCEDURE MATRIZGESTIONLIS(
	------------------------------------------------------------------------------------------------------------------
	-- @nombre:			MATRIZGESTIONLIS
	-- @autor:			DINERO INMEDIATO
	-- @fecha:			15/12/2020
	-- @version:		1.0
	-- @descripcion:	Store procedure para consultar informacion del indicador de pmatriz de indicadores de gestion(OPERACIONES)
	------------------------------------------------------------------------------------------------------------------|
		
	@Par_NumLis			INT,					-- Numero de lista
	@Par_SucursalID		VARCHAR(200),			-- Numero de lista
	@Par_AliadoID		VARCHAR(200),			-- Numero de lista
		
	@Par_Anio			INT,					--Filtrar por a�o
	@Par_Mes			INT						--filtrar por mes
)
AS
BEGIN
	
	-- Declaracion de constantes 
	DECLARE @Var_ListaSuc			INT,		--Listado de sucursales
			
			@ENTERO_VACIO			INT,				-- Entero vacio
			@CADENA_VACIA			CHAR(1),			-- Cadena vacia
			@Var_ListaBaseAll		INT,
			@Var_ListaBaseSuc		INT,
			@Var_ListatipoProdAll	INT,
			@Var_ListaAliadosAll	INT,
			@Var_ListaFamiliadAll	INT,
			@Var_ListaDiaHabil		INT,
			@Var_ListaIngresoVenta	INT,				--Ingreso de ventas para los apartados, joyeria y celular
			@Var_ListaCartera		INT,				--Listado de la cartera para aparatos, celulares y joyeria.
			@Var_ListaAliXSuc		INT,
			--POR MES
			@Var_CarteraMes			INT,				--Listado de la cartera para aparatos, celulares y joyeria DEL MES ANTERIOR.
			@Var_IVentaMes			INT,				--Listado de los ingresos de ventas de aparatos, celulares y joreria con el total del mes anterior
			@Var_ListaBaseMes		INT,
			--POR ALIADO
			@Var_ListaAliadoAliado	INT,				--Listadi de los aliados al seleccionar la sucursal
			@Var_ListaBaseAliado	INT,
			@Var_ListaBaseAMA		INT,
			@Var_ListaIngAliado		INT,
			@Var_ListaIngAliadoMA	INT,
			@Var_ListaAliadoMA		INT,
			--POR PRODUCTO
			@Var_ListaIngProducto	INT,
			--POR PRODUCTO MES ANTERIOR
			@Var_ListaIngProdMA		INT,
			@Var_ListaAnio			INT,
			@Var_ListaACart			INT,
			@Var_ListaAIngVtaACJ	INT,
			--POR A�O
			@Var_ListaMetaAnio		INT,
			@Var_Suc				INT
			
	-- Declaracion de variables 
	DECLARE	@Var_CapitalVigente		INT,				--  capital vigente para el listado de la cartera.
			@Var_Tipo				VARCHAR(100),		--  tipo para el listadoo de la cartera.
			@Var_FechActual			DATETIME,
			@Var_sp					VARCHAR(200),
			@Var_DiaAnterior		DATETIME
			
			
	-- Asignacion de constantes 
	SELECT	@Var_ListaSuc			=	1,
			
			@ENTERO_VACIO			=	0,				-- Asignacion de entero vacio
			@CADENA_VACIA			=	'',				-- Asignacion de cadena vacia
			@Var_ListaBaseAll		=	2,
			@Var_ListaBaseSuc		=	3,
			@Var_ListatipoProdAll	=	4,
			@Var_ListaAliadosAll	=	5,
			@Var_ListaFamiliadAll	=	6,
			@Var_ListaDiaHabil		=	7,
			@Var_ListaIngresoVenta	=	8,
			@Var_ListaCartera		=	9,
			@Var_ListaAliXSuc		=	10,
			--POR MES
			@Var_CarteraMes			=	17,
			@Var_IVentaMes			=	18,
			@Var_ListaBaseMes		=	19,
			--POR ALIADO
			@Var_ListaAliadoAliado	=	20,
			@Var_ListaBaseAliado	=	21,
			@Var_ListaBaseAMA		=	22,
			@Var_ListaIngAliado		=	23,		--listad de Ingreso apartados, celulares, y joyeria
			@Var_ListaIngAliadoMA	=	24,
			@Var_ListaAliadoMA		=	25,
			--POR PRODUCTO
			@Var_ListaIngProducto	=	30,
			--POR MES ANTERIOR
			@Var_ListaIngProdMA		=	31,
			--POR A�O
			@Var_ListaAnio			=	40,
			@Var_ListaACart			=	41,
			@Var_ListaAIngVtaACJ	=	42,
			@Var_ListaMetaAnio		=	43,
			@Var_Suc				=	44

			
			
	---------------	 *** INDICADOR DE MATRIZ DE INDICADORES DE GESTION(DIARIO)
	
	IF @Par_NumLis = @Var_ListaSuc
	BEGIN	      			
		--SELECT NomSuc 
		--	FROM dbo.IndPrestamos
		--	group by NomSuc
		SELECT DISTINCT(AnnEmp), NomSuc,  CveSuc FROM dbo.InformeGerencialMovttos
			WHERE  AnnEmp = 2020
			ORDER BY NomSuc ASC
	END	
	
	--LISTADO DE LA BASE EN EL CATALOGO DE MANTENIMIENTO DE BASE
	IF @Par_NumLis = @Var_ListaBaseAll
	BEGIN
	
		SELECT B.BaseID, B.NomIndicador, B.ValBase, B.SucursalID, B.Mes, ISNULL(e.Em_Nombre, 'Todos') as AliadoID--, B.AliadoID,
		,(CASE B.Mes 
			WHEN 1 THEN 'Enero'
			WHEN 2 THEN 'Febrero'
			WHEN 3 THEN 'Marzo'
			WHEN 4 THEN 'Abril'
			WHEN 5 THEN 'Mayo'
			WHEN 6 THEN 'Junio'
			WHEN 7 THEN 'Julio'
			WHEN 8 THEN 'Agosto'
			WHEN 9 THEN 'Septiembre'
			WHEN 10 THEN 'Octubre'
			WHEN 11 THEN 'Noviembre'
			WHEN 12 THEN 'Diciembre'
			END) AS	NomMes, B.anio, ISNULL(C.Descripcion,'') As Descripcion FROM dbo.BASE B
			LEFT JOIN dbo.COMENTARIO C ON B.BaseID = C.BaseID
			left join DBDIN.dbo.Empleado e on B.AliadoID= e.Em_UserDef_1
			ORDER BY B.BaseID ASC
	END	
	
	--LISTADO DE LAS BASES POR SUCURSAL EN EL FILTRO DE BASE EN LA VISTA DE INDICADORES(PRINCIPAL)
	IF @Par_NumLis = @Var_ListaBaseSuc
	BEGIN
	
		SELECT B.BaseID, B.NomIndicador, B.ValBase, B.SucursalID, B.AliadoID, B.Mes, 
			(CASE Mes 
			WHEN 1 THEN 'Enero'
			WHEN 2 THEN 'Febrero'
			WHEN 3 THEN 'Marzo'
			WHEN 4 THEN 'Abril'
			WHEN 5 THEN 'Mayo'
			WHEN 6 THEN 'Junio'
			WHEN 7 THEN 'Julio'
			WHEN 8 THEN 'Agosto'
			WHEN 9 THEN 'Septiembre'
			WHEN 10 THEN 'Octubre'
			WHEN 11 THEN 'Noviembre'
			WHEN 12 THEN 'Diciembre'
			END) AS NomMes,
			B.anio, ISNULL(C.Descripcion,'') As Descripcion FROM dbo.BASE AS B
			LEFT JOIN dbo.COMENTARIO C ON B.BaseID = C.BaseID
			WHERE B.SucursalID = @Par_SucursalID
			--RESTRINGIR POR A�O ACTUAL Y MES ACTUAL
			AND B.anio = YEAR(GETDATE()) AND Mes= MONTH(GETDATE())
			AND B.AliadoID='Todos'
			ORDER BY B.SucursalID ASC
	END	
	
	--todos los tipos de productos
	IF @Par_NumLis = @Var_ListatipoProdAll
	BEGIN
	
		SELECT Tipo AS Nombre
			FROM dbo.IndPrestamos
			GROUP BY Tipo
			
	END
	
	--todos los aliados
	IF @Par_NumLis = @Var_ListaAliadosAll
	BEGIN
	
		SELECT NomUser AS Nombre
			FROM dbo.IndPrestamos
			GROUP BY NomUser
			
	END
		
	--Lista de los ingresos de ventas de aparatos, celulares y joyeria
	IF @Par_NumLis = @Var_ListaIngresoVenta
	BEGIN
			
				SELECT  Tipo, 
				--SUM(ImpTot) AS Ingreso,
		  --                 isnull((SELECT  SUM(ImpPag) AS Expr1
		  --                  FROM     IndIngresoApartados
		  --                  WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod) AND (Tipo = a.Tipo)),0) AS IngApartado,
				SUM(ImpTot) +
						   isnull((SELECT  SUM(ImpPag) AS Expr1
							FROM     vwIndIngresoApartados
							WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod) AND (Tipo = a.Tipo)),0) as Ingreso                    
		FROM     vwIndIngresoVentas AS a
		WHERE  (AnnMov = YEAR(GETDATE())) AND (MesMov = MONTH(GETDATE())) AND (CveBod = @Par_SucursalID) AND (CveMov IN (8, 37) and fecMov <=(SELECT DATEADD(d,-1,GETDATE())))
		GROUP BY Tipo, AnnMov, MesMov, CveBod
		--union 
		--SELECT  'Total' as Total, SUM(ImpTot) AS Ingreso,
		--                   isnull((SELECT  SUM(ImpPag) AS Expr1
		--                    FROM     IndIngresoApartados
		--                    WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod)),0) AS IngApartado,
		--		SUM(ImpTot) +
		--                   isnull((SELECT  SUM(ImpPag) AS Expr1
		--                    FROM     IndIngresoApartados
		--                    WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod)),0) as IngTotal                    
		--FROM     IndIngresoVentas AS a
		--WHERE  (AnnMov = 2021) AND (MesMov = 1) AND (CveBod = 2) AND (CveMov IN (8, 37))
		--GROUP BY AnnMov, MesMov, CveBod

			
	END
	
	--LISTADO DE LOS DIAS HABILES POR SUCURSAL EN EL CATALOGO DE MANTENIMIENTO
	IF @Par_NumLis = @Var_ListaDiaHabil
	BEGIN
	
		--SELECT FolioID, 
		--	(CASE Mes 
		--	WHEN 1 THEN 'Enero' + ' ' + CAST(A�o AS VARCHAR(200))
		--	WHEN 2 THEN 'Febrero' + ' ' + CAST(A�o AS VARCHAR(200))
		--	WHEN 3 THEN 'Marzo' + ' ' + CAST(A�o AS VARCHAR(200))
		--	WHEN 4 THEN 'Abril' + ' ' + CAST(A�o AS VARCHAR(200))
		--	WHEN 5 THEN 'Mayo' + ' ' + CAST(A�o AS VARCHAR(200))
		--	WHEN 6 THEN 'Junio' + ' ' + CAST(A�o AS VARCHAR(200))
		--	WHEN 7 THEN 'Julio' + ' ' + CAST(A�o AS VARCHAR(200))
		--	WHEN 8 THEN 'Agosto' + ' ' + CAST(A�o AS VARCHAR(200))
		--	WHEN 9 THEN 'Septiembre' + ' ' + CAST(A�o AS VARCHAR(200))
		--	WHEN 10 THEN 'Octubre' + ' ' + CAST(A�o AS VARCHAR(200))
		--	WHEN 11 THEN 'Noviembre' + ' ' + CAST(A�o AS VARCHAR(200))
		--	WHEN 12 THEN 'Diciembre' + ' ' + CAST(A�o AS VARCHAR(200))
		--	END) AS NomMes,FechaInhabil, SucursalID FROM dbo.DIASHABILES
		SELECT SucursalID,
				(CASE Mes 
					WHEN 1 THEN 'Enero' + ' ' + CAST(A�o AS VARCHAR(200))
					WHEN 2 THEN 'Febrero' + ' ' + CAST(A�o AS VARCHAR(200))
					WHEN 3 THEN 'Marzo' + ' ' + CAST(A�o AS VARCHAR(200))
					WHEN 4 THEN 'Abril' + ' ' + CAST(A�o AS VARCHAR(200))
					WHEN 5 THEN 'Mayo' + ' ' + CAST(A�o AS VARCHAR(200))
					WHEN 6 THEN 'Junio' + ' ' + CAST(A�o AS VARCHAR(200))
					WHEN 7 THEN 'Julio' + ' ' + CAST(A�o AS VARCHAR(200))
					WHEN 8 THEN 'Agosto' + ' ' + CAST(A�o AS VARCHAR(200))
					WHEN 9 THEN 'Septiembre' + ' ' + CAST(A�o AS VARCHAR(200))
					WHEN 10 THEN 'Octubre' + ' ' + CAST(A�o AS VARCHAR(200))
					WHEN 11 THEN 'Noviembre' + ' ' + CAST(A�o AS VARCHAR(200))
					WHEN 12 THEN 'Diciembre' + ' ' + CAST(A�o AS VARCHAR(200))
					END) AS NomMes,
					COUNT(FECHAINHABIL) as DiaInhabil
				FROM dbo.DIASHABILES AS D 
				group by Mes, A�o, SucursalID
		
	END
	
	--Lista de la cartera de aparatos, celulares y joyeria
	IF @Par_NumLis = @Var_ListaCartera
	BEGIN
		
			IF ((SELECT COUNT(*) from [vwIndCartera] where AnnCar=YEAR(GETDATE()) and MesCar=month(GETDATE()) and DiaCar=day(GETDATE()))=0) --Hay resultados
			BEGIN
				SELECT @Var_FechActual = (SELECT CONVERT(DATETIME,CONVERT(VARCHAR(10), GETDATE(), 112)+ ' ' + '23:59:59.00') AS FECHACTUAL )
				
				EXECUTE sp_getcortecajaplazos3 @Var_FechActual,99
				 
				--SELECCIONAR EL CAPITAL VENCIDO Y EL TIPO
				SELECT CapitalVigente, Tipo
					FROM [vwIndCartera] where AnnCar=YEAR(GETDATE()) and MesCar=month(GETDATE()) 
					--and DiaCar=day(GETDATE())
					and DiaCar=day((SELECT DATEADD(d,-1,GETDATE())))
			END
			ELSE
			BEGIN
			--SELECCIONAR EL CAPITAL VENCIDO Y EL TIPO
			SELECT CapitalVigente, Tipo
					FROM [vwIndCartera] where AnnCar=YEAR(GETDATE()) and MesCar=month(GETDATE()) --and DiaCar=day(GETDATE())
					and DiaCar=day((SELECT DATEADD(d,-1,GETDATE())))
			END
			
	END
	
		--Lista de aliados por sucursal
	IF @Par_NumLis = @Var_ListaAliXSuc
	BEGIN
		
			SELECT NomUser AS Nombre
				FROM dbo.IndPrestamos
				where NomSuc=@Par_SucursalID
				GROUP BY NomUser, NomSuc
			
	END
	
		---------------	 *** INDICADOR DE MATRIZ DE INDICADORES DE GESTION(MES ANTERIOR)
	
	--Lista de la cartera de aparatos, celulares y joyeria
	IF @Par_NumLis = @Var_CarteraMes
	BEGIN
			--mes anterior 
			SELECT @Var_FechActual = (SELECT DATEADD(dd, DATEPART(dd, GETDATE())* -1,  GETDATE()))
			--mes anterior del mes de arriba
			--SELECT CapitalVigente, Tipo
			--		FROM [vwIndCartera] where AnnCar=YEAR(@Var_FechActual)
			--		and MesCar=MONTH(@Var_FechActual) and DiaCar=day(@Var_FechActual)
	
		IF ((SELECT COUNT(*) from [vwIndCartera] where AnnCar=YEAR(@Var_FechActual) and MesCar=month(@Var_FechActual) and DiaCar=day(@Var_FechActual))=0) --Hay resultados
			BEGIN
				--SELECT @Var_FechActual = (SELECT CONVERT(DATETIME,CONVERT(VARCHAR(10), GETDATE(), 112)+ ' ' + '23:59:59.00') AS FECHACTUAL )
				
				EXECUTE sp_getcortecajaplazos3 @Var_FechActual,99
				 
				--SELECCIONAR EL CAPITAL VENCIDO Y EL TIPO
				SELECT CapitalVigente, Tipo
					FROM [vwIndCartera] where AnnCar=YEAR(@Var_FechActual) and MesCar=month(@Var_FechActual) and DiaCar=day(@Var_FechActual)
			END
			ELSE
			BEGIN
			--SELECCIONAR EL CAPITAL VENCIDO Y EL TIPO
			SELECT CapitalVigente, Tipo
					FROM [vwIndCartera] where AnnCar=YEAR(@Var_FechActual) and MesCar=month(@Var_FechActual) and DiaCar=day(@Var_FechActual)
			END
			
	END
	
	--Lista de los ingresos de ventas de aparatos, celulares y joyeria
	IF @Par_NumLis = @Var_IVentaMes
	BEGIN
	
	
			SELECT @Var_FechActual =(DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
	
	
			SELECT  Tipo, 
				--SUM(ImpTot) AS Ingreso,
		  --                 isnull((SELECT  SUM(ImpPag) AS Expr1
		  --                  FROM     IndIngresoApartados
		  --                  WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod) AND (Tipo = a.Tipo)),0) AS IngApartado,
				SUM(ImpTot) +
						   isnull((SELECT  SUM(ImpPag) AS Expr1
							FROM     vwIndIngresoApartados
							WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod) AND (Tipo = a.Tipo)),0) as Ingreso                    
		FROM     vwIndIngresoVentas AS a
		WHERE  (AnnMov = YEAR(@Var_FechActual)) AND (MesMov = MONTH(@Var_FechActual)) AND (CveBod = @Par_SucursalID) AND (CveMov IN (8, 37))
		GROUP BY Tipo, AnnMov, MesMov, CveBod
		--union 
		--SELECT  'Total' as Total, SUM(ImpTot) AS Ingreso,
		--                   isnull((SELECT  SUM(ImpPag) AS Expr1
		--                    FROM     IndIngresoApartados
		--                    WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod)),0) AS IngApartado,
		--		SUM(ImpTot) +
		--                   isnull((SELECT  SUM(ImpPag) AS Expr1
		--                    FROM     IndIngresoApartados
		--                    WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod)),0) as IngTotal                    
		--FROM     IndIngresoVentas AS a
		--WHERE  (AnnMov = 2021) AND (MesMov = 1) AND (CveBod = 2) AND (CveMov IN (8, 37))
		--GROUP BY AnnMov, MesMov, CveBod

			
	END
	
		--LISTADO DE LAS BASES POR SUCURSAL EN EL FILTRO DE BASE EN LA VISTA DE INDICADORES(PRINCIPAL)
	IF @Par_NumLis = @Var_ListaBaseMes
	BEGIN
	
		SELECT B.BaseID, B.NomIndicador, B.ValBase, B.SucursalID, B.AliadoID, B.Mes, 
			(CASE B.Mes 
			WHEN 1 THEN 'Enero'
			WHEN 2 THEN 'Febrero'
			WHEN 3 THEN 'Marzo'
			WHEN 4 THEN 'Abril'
			WHEN 5 THEN 'Mayo'
			WHEN 6 THEN 'Junio'
			WHEN 7 THEN 'Julio'
			WHEN 8 THEN 'Agosto'
			WHEN 9 THEN 'Septiembre'
			WHEN 10 THEN 'Octubre'
			WHEN 11 THEN 'Noviembre'
			WHEN 12 THEN 'Diciembre'
			END) AS NomMes,
			B.anio, ISNULL(C.Descripcion,'') As Descripcion  FROM dbo.BASE AS B
			LEFT JOIN dbo.COMENTARIO C ON B.BaseID = C.BaseID
			WHERE B.SucursalID = @Par_SucursalID
			--RESTRINGIR POR A�O ACTUAL Y MES ACTUAL
			AND B.anio = @Par_Anio
			AND B.Mes= @Par_Mes
			AND B.AliadoID='Todos'
			ORDER BY B.BaseID ASC
	END	
		
	---------------	 *** INDICADOR DE MATRIZ DE INDICADORES DE GESTION(POR ALIADO)
	IF @Par_NumLis = @Var_ListaAliadoAliado
	BEGIN
	--listado de los aliados de la sucursal , seleccionar la sucursal y se despliega los aliados	
		--Empleados de la tabla de la sucursal seleccionad, tabla empe�os de cada servidor
						
			SELECT  login as CveUser, NomUser AS Nombre
				FROM     vwEmpMovttoGral
				WHERE  (YEAR(FecIni) = YEAR(GETDATE())) 
				AND (MONTH(FecIni) = MONTH(GETDATE())) AND (Status <> 'Cancelada')
				GROUP BY  login, NomUser
			
			
			--select @Par_SucursalID= (right('000000' + @Par_SucursalID ,4))
			
			--select e.Em_UserDef_1 as CveUser, e.Em_Nombre +' - '+ p.Pe_Descripcion As Nombre, e.Sc_Cve_Sucursal from DBDIN.dbo.Empleado e
			--	inner join DBDIN.dbo.Puesto_Empleado p on e.Pe_Cve_Puesto_Empleado = p.Pe_Cve_Puesto_Empleado
			--	-- where e.Sc_Cve_Sucursal ='0006' and e.Es_Cve_Estado='AC'	
			-- where e.Sc_Cve_Sucursal =@Par_SucursalID and e.Es_Cve_Estado='AC'
	
	END
	
	--Lista de los ingresos de ventas de aparatos, celulares y joyeria (POR ALIADO)
	IF @Par_NumLis = @Var_ListaIngAliado
	BEGIN
			SELECT  Tipo,
                  
						SUM(ImpTot) +
								   isnull((SELECT  SUM(ImpPag) AS Expr1
									FROM     vwIndIngresoApartados
									WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod) AND (Tipo = a.Tipo)),0) as Ingreso                    
				FROM     vwIndIngresoVentas AS a
				WHERE  (AnnMov = YEAR(GETDATE())) AND (MesMov = MONTH(GETDATE())) AND (CveBod = @Par_SucursalID) AND (CveMov IN (8, 37)) and LOGIN = @Par_AliadoID
				GROUP BY Tipo, AnnMov, MesMov, CveBod
				union 
				SELECT  'Total' as Total,
						SUM(ImpTot) +
								   isnull((SELECT  SUM(ImpPag) AS Expr1
									FROM     vwIndIngresoApartados
									WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod)),0) as Ingreso                    
				FROM     vwIndIngresoVentas AS a
				WHERE  (AnnMov = YEAR(GETDATE())) AND (MesMov = MONTH(GETDATE())) AND (CveBod = @Par_SucursalID) AND (CveMov IN (8, 37)) and LOGIN = @Par_AliadoID
				GROUP BY AnnMov, MesMov, CveBod
				
			
	END
	
	IF @Par_NumLis = @Var_ListaBaseAliado
	BEGIN
	--listado de las metas por aliado en la pesta�a aliado
			SELECT BaseID, NomIndicador, ValBase, SucursalID, AliadoID, Mes, 
			(CASE Mes 
			WHEN 1 THEN 'Enero'
			WHEN 2 THEN 'Febrero'
			WHEN 3 THEN 'Marzo'
			WHEN 4 THEN 'Abril'
			WHEN 5 THEN 'Mayo'
			WHEN 6 THEN 'Junio'
			WHEN 7 THEN 'Julio'
			WHEN 8 THEN 'Agosto'
			WHEN 9 THEN 'Septiembre'
			WHEN 10 THEN 'Octubre'
			WHEN 11 THEN 'Noviembre'
			WHEN 12 THEN 'Diciembre'
			END) AS NomMes,
			anio FROM dbo.BASE
			WHERE SucursalID = @Par_SucursalID
			--RESTRINGIR POR A�O ACTUAL Y MES ACTUAL
			AND anio = YEAR(GETDATE())
			AND Mes= MONTH(GETDATE())
			AND AliadoID=@Par_AliadoID
			ORDER BY BaseID ASC
			
	END
	
	IF @Par_NumLis = @Var_ListaBaseAMA
	BEGIN
	--listado de las metas por aliado en la pesta�a aliado
	SELECT @Var_FechActual =(DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
	
			SELECT BaseID, NomIndicador, ValBase, SucursalID, AliadoID, Mes, 
			(CASE Mes 
			WHEN 1 THEN 'Enero'
			WHEN 2 THEN 'Febrero'
			WHEN 3 THEN 'Marzo'
			WHEN 4 THEN 'Abril'
			WHEN 5 THEN 'Mayo'
			WHEN 6 THEN 'Junio'
			WHEN 7 THEN 'Julio'
			WHEN 8 THEN 'Agosto'
			WHEN 9 THEN 'Septiembre'
			WHEN 10 THEN 'Octubre'
			WHEN 11 THEN 'Noviembre'
			WHEN 12 THEN 'Diciembre'
			END) AS NomMes,
			anio FROM dbo.BASE
			WHERE SucursalID = @Par_SucursalID
			--RESTRINGIR POR A�O ACTUAL Y MES ACTUAL
			AND anio = YEAR(@Var_FechActual)
			AND Mes= MONTH(@Var_FechActual)
			AND AliadoID=@Par_AliadoID
			ORDER BY BaseID ASC
			
	END			
	
	--Lista de los ingresos de ventas de aparatos, celulares y joyeria (POR ALIADO DEL MES ANTERIOR)
	IF @Par_NumLis = @Var_ListaIngAliadoMA
	BEGIN
	
	SELECT @Var_FechActual =(DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
	
			SELECT  Tipo, --SUM(ImpTot) AS Ingreso,
                   --isnull((SELECT  SUM(ImpPag) AS Expr1
                   -- FROM     vwIndIngresoApartados
                   -- WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod) AND (Tipo = a.Tipo)),0) AS IngApartado,
						SUM(ImpTot) +
								   isnull((SELECT  SUM(ImpPag) AS Expr1
									FROM     vwIndIngresoApartados
									WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod) AND (Tipo = a.Tipo)),0) as Ingreso                    
				FROM     vwIndIngresoVentas AS a
				WHERE  (AnnMov = YEAR(@Var_FechActual)) AND (MesMov = MONTH(@Var_FechActual)) AND (CveBod = @Par_SucursalID) AND (CveMov IN (8, 37)) and LOGIN = @Par_AliadoID
				GROUP BY Tipo, AnnMov, MesMov, CveBod
				union 
				SELECT  'Total' as Total, --SUM(ImpTot) AS Ingreso,
								 --  isnull((SELECT  SUM(ImpPag) AS Expr1
									--FROM     vwIndIngresoApartados
									--WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod)),0) AS IngApartado,
						SUM(ImpTot) +
								   isnull((SELECT  SUM(ImpPag) AS Expr1
									FROM     vwIndIngresoApartados
									WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod)),0) as Ingreso                    
				FROM     vwIndIngresoVentas AS a
				WHERE  (AnnMov = YEAR(@Var_FechActual)) AND (MesMov = MONTH(@Var_FechActual)) AND (CveBod = @Par_SucursalID) AND (CveMov IN (8, 37)) and LOGIN = @Par_AliadoID
				GROUP BY AnnMov, MesMov, CveBod
			
	END
	
	
	--Alidos del mes anterior
	IF @Par_NumLis =	@Var_ListaAliadoMA
	BEGIN
	
		SELECT @Var_FechActual =(DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
	
		SELECT  login as CveUser, NomUser AS Nombre
			FROM     vwEmpMovttoGral
			WHERE  (YEAR(FecIni) = YEAR(@Var_FechActual)) 
			AND (MONTH(FecIni) = MONTH(@Var_FechActual)) AND (Status <> 'Cancelada')
			GROUP BY  login, NomUser
	END

	---------------	 *** INDICADOR DE MATRIZ DE INDICADORES DE GESTION(POR PRODUCTO)
	--	***	indicadores del mes actual:
	
	--todas las familias
	IF @Par_NumLis = @Var_ListaFamiliadAll
	BEGIN
	
		--SELECT DISTINCT(Producto) AS Nombre
		--	FROM dbo.IndPrestamos
					--servidor 2
		--SELECT CveTipProd, Producto AS Nombre
		--			FROM dbo.IndPrestamos
		--			where NomSuc =@Par_SucursalID
		--			and AnnEmp=YEAR(GETDATE())
		--			and MesEmp=MONTH(GETDATE())
		--			group by Producto, CveTipProd
		
		--servidor de empe�os		
		SELECT CveTipProd, Producto AS Nombre, CveSuc, AnnEmp, MesEmp FROM vwEmpMovttoGral
					WHERE CveSuc=@Par_SucursalID
					AND AnnEmp= YEAR(GETDATE())
					AND MesEmp = MONTH(GETDATE())
					GROUP BY Producto, AnnEmp, MesEmp, CveTipProd, CveSuc
			
	END
	
	--Lista de los ingresos de ventas de aparatos, celulares y joyeria (POR ALIADO) + ingreso total (empe�o - ventas)
	IF @Par_NumLis = @Var_ListaIngProducto
	BEGIN			
			SELECT  Tipo, --SUM(ImpTot) AS Ingreso,
                   --isnull((SELECT  SUM(ImpPag) AS Expr1
                   -- FROM     vwIndIngresoApartados
                   -- WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod) AND (Tipo = a.Tipo)),0) AS IngApartado,
						SUM(ImpTot) + isnull((SELECT  SUM(ImpPag) AS Expr1
												FROM     vwIndIngresoApartados
												WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod) AND (Tipo = a.Tipo)),0) as Ingreso                    
				FROM     vwIndIngresoVentas AS a
				WHERE  (AnnMov = YEAR(GETDATE())) AND (MesMov = MONTH(GETDATE())) AND (CveBod = @Par_SucursalID) AND (CveMov IN (8, 37)) and DESFAM = 'PLATA'
				GROUP BY Tipo, AnnMov, MesMov, CveBod
				union 
				SELECT  'Total' as Total,-- SUM(ImpTot) AS Ingreso,
								   --isnull((SELECT  SUM(ImpPag) AS Expr1
								   -- FROM     vwIndIngresoApartados
								   -- WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod)),0) AS IngApartado,
						SUM(ImpTot) +isnull((SELECT  SUM(ImpPag) AS Expr1
												FROM     vwIndIngresoApartados
												WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod)),0) as Ingreso                    
				FROM     vwIndIngresoVentas AS a
				WHERE  (AnnMov = YEAR(GETDATE())) AND (MesMov =MONTH(GETDATE())) AND (CveBod = @Par_SucursalID) AND (CveMov IN (8, 37)) and DESFAM = 'PLATA'
				GROUP BY AnnMov, MesMov, CveBod
			
	END
	
	--	***	indicadores del mes anterior
	--Lista de los ingresos de ventas de aparatos, celulares y joyeria (POR ALIADO DEL MES ANTERIOR) 
	IF @Par_NumLis = @Var_ListaIngProdMA
	BEGIN			
			SELECT @Var_FechActual =(DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
			
			SELECT  Tipo, --SUM(ImpTot) AS Ingreso,
                   --isnull((SELECT  SUM(ImpPag) AS Expr1
                   -- FROM     vwIndIngresoApartados
                   -- WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod) AND (Tipo = a.Tipo)),0) AS IngApartado,
						SUM(ImpTot) + isnull((SELECT  SUM(ImpPag) AS Expr1
												FROM     vwIndIngresoApartados
												WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod) AND (Tipo = a.Tipo)),0) as Ingreso                    
				FROM     vwIndIngresoVentas AS a
				WHERE  (AnnMov = YEAR(@Var_FechActual)) AND (MesMov = MONTH(@Var_FechActual)) AND (CveBod = @Par_SucursalID) AND (CveMov IN (8, 37)) and DESFAM = @Par_AliadoID
				GROUP BY Tipo, AnnMov, MesMov, CveBod
				union 
				SELECT  'Total' as Total,-- SUM(ImpTot) AS Ingreso,
								   --isnull((SELECT  SUM(ImpPag) AS Expr1
								   -- FROM     vwIndIngresoApartados
								   -- WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod)),0) AS IngApartado,
						SUM(ImpTot) +isnull((SELECT  SUM(ImpPag) AS Expr1
												FROM     vwIndIngresoApartados
												WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod)),0) as Ingreso                    
				FROM     vwIndIngresoVentas AS a
				WHERE  (AnnMov = YEAR(@Var_FechActual)) AND (MesMov =MONTH(@Var_FechActual)) AND (CveBod = @Par_SucursalID) AND (CveMov IN (8, 37)) and DESFAM = @Par_AliadoID
				GROUP BY AnnMov, MesMov, CveBod
			
	END
		
	---------------	 *** INDICADOR DE MATRIZ DE INDICADORES DE GESTION(ANUAL)
	IF @Par_NumLis = @Var_ListaAnio
		BEGIN
		
			SELECT CveSuc, AnnEmp FROM vwEmpMovttoGral 
					GROUP BY AnnEmp, CveSuc
							
		END

	--Lista de la cartera de aparatos, celulares y joyeria
	IF @Par_NumLis = @Var_ListaACart
	BEGIN
	
			SELECT SUM(CapitalVigente) AS CapitalVigente, Tipo
					FROM [vwIndCartera] WHERE AnnCar=@Par_Anio 
					GROUP BY Tipo				
			
	END
	
	--Lista de ingreso venta aparatos, celular y joyeria
	IF @Par_NumLis = @Var_ListaAIngVtaACJ
	BEGIN
	
			SELECT  Tipo, 
				--SUM(ImpTot) AS Ingreso,
		  --                 isnull((SELECT  SUM(ImpPag) AS Expr1
		  --                  FROM     IndIngresoApartados
		  --                  WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod) AND (Tipo = a.Tipo)),0) AS IngApartado,
				SUM(ImpTot) +
						   isnull((SELECT  SUM(ImpPag) AS Expr1
							FROM     vwIndIngresoApartados
							WHERE  (AnnMov = a.AnnMov) AND (BodVta = a.CveBod) AND (Tipo = a.Tipo)),0) as Ingreso                    
		FROM     vwIndIngresoVentas AS a
		WHERE  (AnnMov = @Par_Anio) AND (CveBod = @Par_SucursalID) AND (CveMov IN (8, 37) )
		GROUP BY Tipo, AnnMov,  CveBod
		--union 
		--SELECT  'Total' as Total, SUM(ImpTot) AS Ingreso,
		--                   isnull((SELECT  SUM(ImpPag) AS Expr1
		--                    FROM     IndIngresoApartados
		--                    WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod)),0) AS IngApartado,
		--		SUM(ImpTot) +
		--                   isnull((SELECT  SUM(ImpPag) AS Expr1
		--                    FROM     IndIngresoApartados
		--                    WHERE  (AnnMov = a.AnnMov) AND (MesMov = a.MesMov) AND (BodVta = a.CveBod)),0) as IngTotal                    
		--FROM     IndIngresoVentas AS a
		--WHERE  (AnnMov = 2021) AND (MesMov = 1) AND (CveBod = 2) AND (CveMov IN (8, 37))
		--GROUP BY AnnMov, MesMov, CveBod
			
			
	END
	
	--Lista de ingreso venta aparatos, celular y joyeria
	IF @Par_NumLis = @Var_ListaMetaAnio
	BEGIN
	
			SELECT NomIndicador, ValBase FROM AnaliticaInd.dbo.BASE
			WHERE Mes =0
			AND anio = @Par_Anio
			AND SucursalID =@Par_SucursalID
			
			
	END
	--SUCURSALES PARA LOS INDICADORES DIARIOS //SUCURSALES DEPENDIENDO DEL USUARI QUE INICIA SESION
	IF @Par_NumLis = @Var_Suc
	BEGIN
	
			SELECT SucursalID,Sucursal FROM  PermisoOperaciones
			WHERE UsuarioID=@Par_AliadoID
			
			
	END
	


END
