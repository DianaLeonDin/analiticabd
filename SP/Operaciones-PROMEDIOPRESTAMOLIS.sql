IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[DBO].[PROMEDIOPRESTAMOLIS]') AND type IN ('P'))
	DROP PROCEDURE PROMEDIOPRESTAMOLIS
GO

CREATE PROCEDURE PROMEDIOPRESTAMOLIS(
	------------------------------------------------------------------------------------------------------------------
	-- @nombre:			PROMEDIOPRESTAMOLIS
	-- @autor:			DINERO INMEDIATO
	-- @fecha:			08/12/2020
	-- @version:		1.0
	-- @descripcion:	Store procedure para consultar informacion del indicador de promedio prestamo(OPERACIONES)
	------------------------------------------------------------------------------------------------------------------|
		
	@Par_NumLis			INT, 					-- Numero de lista
	
	@Par_Sucursal		VARCHAR(50),
	@Par_Anio			INT,
	@Par_Mes			INT,
	@Par_Aliado			VARCHAR(300),
	@Par_TipoProd		VARCHAR(300),
	@Par_Producto		VARCHAR(300)
	
)
AS
BEGIN
	
	-- Declaracion de constantes 
	DECLARE @Var_ListaSuc			INT,		--Listado de sucursales
			@Var_ListaA�o			INT,
			@Var_ListaMes			INT,
			@Var_ListaAliado		INT,
			@Var_ListatipoProd		INT,
			@Var_ListaProducto		INT,
			@Var_PromedioAnio		INT,
			@Var_PromedioMes		INT,
			@Var_promedio			decimal,
			@ENTERO_VACIO			INT,				-- Entero vacio
			@CADENA_VACIA			CHAR(1),			-- Cadena vacia
			
			@Var_ListaAliados		INT,
			@Var_ListaProductos		INT
	

	-- Asignacion de constantes 
	SELECT	@Var_ListaSuc			=	1,
			@Var_ListaA�o			=	2,
			@Var_ListaMes			=	3,
			@Var_ListaAliado		=	4,
			@Var_ListatipoProd		=	5,
			@Var_ListaProducto		=	6,
			@Var_PromedioAnio		=	7,
			@Var_PromedioMes		=	8,
			@ENTERO_VACIO			=	0,				-- Asignacion de entero vacio
			@CADENA_VACIA			=	'',				-- Asignacion de cadena vacia
			
			@Var_ListaAliados		=	9,				-- Asignacion de cadena vacia
			@Var_ListaProductos		=	10				-- Asignacion de cadena vacia
			

			
			
	IF @Par_NumLis = @Var_ListaSuc
	BEGIN
	      			
		--SELECT NomSuc 
		--	FROM dbo.InformeGerencialMovttos
		--	group by NomSuc
		--	order by NomSuc asc
		SELECT REPLACE(LTRIM(REPLACE(Sc_Cve_Sucursal, '0', ' ')),' ', '0') SucursalID, Sc_Descripcion AS NomSuc FROM DBDIN.DBO.Sucursal
			WHERE Gs_Cve_Grupo_Sucursal='0001'
			AND Sc_Descripcion NOT LIKE '%VENTAS%'
			AND Sc_Descripcion NOT LIKE '%OFICINAS%'


	END	
	
	IF @Par_NumLis = @Var_ListaA�o
	BEGIN
	      			
		--SELECT DISTINCT(AnnEmp) 
		--	FROM dbo.IndPrestamos WHERE NomSuc=@Par_Sucursal 
		SELECT DISTINCT(AnnEmp) 
			FROM dbo.InformeGerencialMovttos WHERE NomSuc=@Par_Sucursal 
			order by AnnEmp desc
			
	END	
	
	IF @Par_NumLis = @Var_ListaMes
	BEGIN
	      			
		SELECT DISTINCT(MesEmp),
		(SELECT  CASE 
			WHEN MesEmp=1 THEN 'ENERO'
			WHEN MesEmp=2 THEN 'FEBRERO'
			WHEN MesEmp=3 THEN 'MARZO'
			WHEN MesEmp=4 THEN 'ABRIL'
			WHEN MesEmp=5 THEN 'MAYO'
			WHEN MesEmp=6 THEN 'JUNIO'
			WHEN MesEmp=7 THEN 'JULIO'
			WHEN MesEmp=8 THEN 'AGOSTO'
			WHEN MesEmp=9 THEN 'SEPTIEMBRE'
			WHEN MesEmp=10 THEN 'OCTUBRE'
			WHEN MesEmp=11 THEN 'NOVIEMBRE'
			WHEN MesEmp=12 THEN 'DICIEMBRE'
			ELSE ''
		END)AS Mes
			FROM dbo.InformeGerencialMovttos WHERE NomSuc=@Par_Sucursal and AnnEmp=@Par_Anio
			order by MesEmp asc
	END	
	
	IF @Par_NumLis = @Var_ListaAliado
	BEGIN
	     
		SELECT DISTINCT(NomUser)
			FROM dbo.IndPrestamos WHERE NomSuc=@Par_Sucursal and AnnEmp=@Par_Anio
			and MesEmp=@Par_Mes
			
	END	
	
	IF @Par_NumLis = @Var_ListatipoProd
	BEGIN
	     
			
		--SELECT DISTINCT(Tipo)
		--	FROM dbo.IndPrestamos WHERE NomSuc=@Par_Sucursal and AnnEmp=@Par_Anio
		--	and MesEmp=@Par_Mes and NomUser= @Par_Aliado
		SELECT DISTINCT(Tipo)
			FROM dbo.IndPrestamos WHERE NomSuc=@Par_Sucursal and AnnEmp=@Par_Anio
			and MesEmp=@Par_Mes 
			
	END	
	
	IF @Par_NumLis = @Var_ListaProducto
	BEGIN
	
		--SELECT DISTINCT(Producto)
		--	FROM dbo.IndPrestamos WHERE NomSuc=@Par_Sucursal and AnnEmp=@Par_Anio
		--	AND MesEmp=@Par_Mes and NomUser= @Par_Aliado
		--	AND Tipo=@Par_TipoProd
		SELECT DISTINCT(Producto)
			FROM dbo.IndPrestamos WHERE NomSuc=@Par_Sucursal and AnnEmp=@Par_Anio
			AND MesEmp=@Par_Mes 
				
	END	
	
	IF @Par_NumLis = @Var_PromedioAnio
	BEGIN
	
		--Select @Var_promedio=
	
		IF @Par_Aliado ='' and @Par_TipoProd='' and @Par_Producto=''
		begin 
			select distinct AnnEmp,SUM(capital)/SUM(cantidad) as Promedio from dbo.IndPrestamos
				where NomSuc=@Par_Sucursal and AnnEmp between @Par_Anio-2 and @Par_Anio  group by AnnEmp
		end
		
		IF @Par_Aliado !='' and @Par_TipoProd='' and @Par_Producto=''
		begin 
			select distinct AnnEmp,SUM(capital)/SUM(cantidad) as Promedio from dbo.IndPrestamos
				where NomSuc=@Par_Sucursal and AnnEmp between @Par_Anio-2 and @Par_Anio and NomUser= @Par_Aliado  group by AnnEmp
		end
		
		IF @Par_Aliado ='' and @Par_TipoProd!='' and @Par_Producto=''
		begin 
			select distinct AnnEmp,SUM(capital)/SUM(cantidad) as Promedio from dbo.IndPrestamos
				where NomSuc=@Par_Sucursal and AnnEmp between @Par_Anio-2 and @Par_Anio and Tipo= @Par_TipoProd  group by AnnEmp
		end
		
		IF @Par_Aliado ='' and @Par_TipoProd='' and @Par_Producto!=''
		begin 
			select distinct AnnEmp,SUM(capital)/SUM(cantidad) as Promedio from dbo.IndPrestamos
				where NomSuc=@Par_Sucursal and AnnEmp between @Par_Anio-2 and @Par_Anio and Producto= @Par_Producto  group by AnnEmp
		end
				
		IF @Par_Aliado !='' and @Par_TipoProd!='' and @Par_Producto!=''
		begin 
			select distinct AnnEmp,SUM(capital)/SUM(cantidad) as Promedio from dbo.IndPrestamos
				where NomSuc=@Par_Sucursal and AnnEmp between @Par_Anio-2 and @Par_Anio 
				and Producto= @Par_Producto and Tipo=@Par_TipoProd and NomUser= @Par_Aliado   group by AnnEmp
		end
		
		IF @Par_Aliado !='' and @Par_TipoProd!='' and @Par_Producto=''
		begin 
			select distinct AnnEmp,SUM(capital)/SUM(cantidad) as Promedio from dbo.IndPrestamos
				where NomSuc=@Par_Sucursal and AnnEmp between @Par_Anio-2 and @Par_Anio 
				and Tipo=@Par_TipoProd and NomUser= @Par_Aliado   group by AnnEmp
		end	
		
		IF @Par_Aliado !='' and @Par_TipoProd='' and @Par_Producto!=''
		begin 
			select distinct AnnEmp,SUM(capital)/SUM(cantidad) as Promedio from dbo.IndPrestamos
				where NomSuc=@Par_Sucursal and AnnEmp between @Par_Anio-2 and @Par_Anio 
				and NomUser= @Par_Aliado and Producto=@Par_Producto   group by AnnEmp
		end
			
		IF @Par_Aliado ='' and @Par_TipoProd!='' and @Par_Producto!=''
		begin 
			select distinct AnnEmp,SUM(capital)/SUM(cantidad) as Promedio from dbo.IndPrestamos
				where NomSuc=@Par_Sucursal and AnnEmp between @Par_Anio-2 and @Par_Anio 
				and Tipo= @Par_TipoProd and Producto=@Par_Producto   group by AnnEmp
		end
		
				
	END	
	
	IF @Par_NumLis = @Var_PromedioMes
	BEGIN
	
		IF @Par_Mes		=	1
		BEGIN
			IF @Par_Aliado ='' and @Par_TipoProd='' and @Par_Producto=''
		begin 
			
			  select AnnEmp,  MesEmp, 
			  (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp = 1 and AnnEmp = @Par_Anio and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes, sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12,11) and AnnEmp = @Par_Anio -1 and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				  Order by AnnEmp desc ,MesEmp desc
    
		end
		
			IF @Par_Aliado !='' and @Par_TipoProd='' and @Par_Producto=''
			begin 
			
				select AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes
					,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp = 1 and AnnEmp = @Par_Anio and NomUser= @Par_Aliado and NomSuc=@Par_Sucursal 
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp,
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes, sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12,11) and AnnEmp = @Par_Anio -1 and NomUser= @Par_Aliado  and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				  Order by AnnEmp desc ,MesEmp desc
				 
			end
			
			IF @Par_Aliado ='' and @Par_TipoProd!='' and @Par_Producto=''
			begin 
				
				select AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp = 1 and AnnEmp = @Par_Anio and Tipo= @Par_TipoProd and NomSuc=@Par_Sucursal   
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12,11) and AnnEmp = @Par_Anio -1 and Tipo= @Par_TipoProd and NomSuc=@Par_Sucursal  
				 Group by AnnEmp,  MesEmp
				  Order by AnnEmp desc ,MesEmp desc
				 
			end
			
			IF @Par_Aliado ='' and @Par_TipoProd='' and @Par_Producto!=''
			begin 
			
				select AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp = 1 and AnnEmp = @Par_Anio and Producto= @Par_Producto  and NomSuc=@Par_Sucursal  
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12,11) and AnnEmp = @Par_Anio -1 and Producto= @Par_Producto  and NomSuc=@Par_Sucursal 
				 Group by AnnEmp,  MesEmp
				  Order by AnnEmp desc ,MesEmp desc
			end
					
			IF @Par_Aliado !='' and @Par_TipoProd!='' and @Par_Producto!=''
			begin
			
				select AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp = 1 and AnnEmp = @Par_Anio 
				 and Producto= @Par_Producto and Tipo=@Par_TipoProd and NomUser= @Par_Aliado and NomSuc=@Par_Sucursal    
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12,11) and AnnEmp = @Par_Anio -1 and 
				 Producto= @Par_Producto and Tipo=@Par_TipoProd and NomUser= @Par_Aliado  and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				  Order by AnnEmp desc ,MesEmp desc
				
			end
			
			IF @Par_Aliado !='' and @Par_TipoProd!='' and @Par_Producto=''
			begin 
			
				select AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp = 1 and AnnEmp = @Par_Anio 
				 and Tipo=@Par_TipoProd and NomUser= @Par_Aliado   and NomSuc=@Par_Sucursal 
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12,11) and AnnEmp = @Par_Anio -1 and 
				 Tipo=@Par_TipoProd and NomUser= @Par_Aliado  and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				  Order by AnnEmp desc ,MesEmp desc
				 
			end	
			
			IF @Par_Aliado !='' and @Par_TipoProd='' and @Par_Producto!=''
			begin 
			
				select AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp = 1 and AnnEmp = @Par_Anio 
				 and NomUser= @Par_Aliado and Producto=@Par_Producto  and NomSuc=@Par_Sucursal 
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12,11) and AnnEmp = @Par_Anio -1 and 
				 NomUser= @Par_Aliado and Producto=@Par_Producto and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				 --Order by AnnEmp desc, MesEmp desc	
				  Order by AnnEmp desc ,MesEmp desc
				 
			end
				
			IF @Par_Aliado ='' and @Par_TipoProd!='' and @Par_Producto!=''
			begin 
			
				select AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp = 1 and AnnEmp = @Par_Anio 
				 and Tipo= @Par_TipoProd and Producto=@Par_Producto and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12,11) and AnnEmp = @Par_Anio -1 and 
				 Tipo= @Par_TipoProd and Producto=@Par_Producto and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				 --Order by AnnEmp desc, MesEmp desc	
				 Order by AnnEmp desc ,MesEmp desc
				 
			end
		END
		
		IF @Par_Mes		=	2
		BEGIN
			IF @Par_Aliado ='' and @Par_TipoProd='' and @Par_Producto=''
		begin 
			
			  select AnnEmp,  MesEmp, 
			  (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,
					sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp <=2 and AnnEmp = @Par_Anio and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12) and AnnEmp = @Par_Anio -1 and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				 Order by AnnEmp desc, MesEmp desc 
				 
    
		end
		
			IF @Par_Aliado !='' and @Par_TipoProd='' and @Par_Producto=''
			begin 
			
				select AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp <=2 and AnnEmp = @Par_Anio and NomUser= @Par_Aliado  and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12) and AnnEmp = @Par_Anio -1 and NomUser= @Par_Aliado  and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				 Order by AnnEmp desc, MesEmp desc
				
			end
			
			IF @Par_Aliado ='' and @Par_TipoProd!='' and @Par_Producto=''
			begin 
				
				select AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp<=2 and AnnEmp = @Par_Anio and Tipo= @Par_TipoProd and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12) and AnnEmp = @Par_Anio -1 and Tipo= @Par_TipoProd  and NomSuc=@Par_Sucursal 
				 Group by AnnEmp,  MesEmp
				 Order by AnnEmp desc, MesEmp desc
				 
			end
			
			IF @Par_Aliado ='' and @Par_TipoProd='' and @Par_Producto!=''
			begin 
			
				select AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp <=2 and AnnEmp = @Par_Anio and Producto= @Par_Producto and NomSuc=@Par_Sucursal    
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12) and AnnEmp = @Par_Anio -1 and Producto= @Par_Producto and NomSuc=@Par_Sucursal   
				 Group by AnnEmp,  MesEmp
				 Order by AnnEmp desc, MesEmp desc	
			end
					
			IF @Par_Aliado !='' and @Par_TipoProd!='' and @Par_Producto!=''
			begin
			
				select AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp <=2 and AnnEmp = @Par_Anio 
				 and Producto= @Par_Producto and Tipo=@Par_TipoProd and NomUser= @Par_Aliado and NomSuc=@Par_Sucursal    
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12) and AnnEmp = @Par_Anio -1 and 
				 Producto= @Par_Producto and Tipo=@Par_TipoProd and NomUser= @Par_Aliado and NomSuc=@Par_Sucursal 
				 Group by AnnEmp,  MesEmp
				 Order by AnnEmp desc, MesEmp desc	
				 
				
			end
			
			IF @Par_Aliado !='' and @Par_TipoProd!='' and @Par_Producto=''
			begin 
			
				select AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp <=2 and AnnEmp = @Par_Anio 
				 and Tipo=@Par_TipoProd and NomUser= @Par_Aliado and NomSuc=@Par_Sucursal    
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12) and AnnEmp = @Par_Anio -1 and 
				 Tipo=@Par_TipoProd and NomUser= @Par_Aliado  and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				 Order by AnnEmp desc, MesEmp desc	
				 
			end	
			
			IF @Par_Aliado !='' and @Par_TipoProd='' and @Par_Producto!=''
			begin 
			
				select AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp <=2 and AnnEmp = @Par_Anio 
				 and NomUser= @Par_Aliado and Producto=@Par_Producto and NomSuc=@Par_Sucursal  
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12) and AnnEmp = @Par_Anio -1 and 
				 NomUser= @Par_Aliado and Producto=@Par_Producto and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				 Order by AnnEmp desc, MesEmp desc	
				 
			end
				
			IF @Par_Aliado ='' and @Par_TipoProd!='' and @Par_Producto!=''
			begin 
			
				select AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				 where MesEmp <=2 and AnnEmp = @Par_Anio 
				 and Tipo= @Par_TipoProd and Producto=@Par_Producto and NomSuc=@Par_Sucursal  
				 Group by AnnEmp,  MesEmp
				 union all 
				 select AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad) from dbo.IndPrestamos 
				 where MesEmp in (12) and AnnEmp = @Par_Anio -1 and 
				 Tipo= @Par_TipoProd and Producto=@Par_Producto and NomSuc=@Par_Sucursal
				 Group by AnnEmp,  MesEmp
				 Order by AnnEmp desc, MesEmp desc	
				 
			end
		END
		
		IF @Par_Mes		>	2
		BEGIN
			IF @Par_Aliado ='' and @Par_TipoProd='' and @Par_Producto=''
		begin 
			 select top 3 AnnEmp,  MesEmp, (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes, sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				  where MesEmp <=@Par_Mes  and AnnEmp = @Par_Anio and NomSuc=@Par_Sucursal
				  Group by AnnEmp,  MesEmp
				  --Order by AnnEmp desc, MesEmp desc 
				  Order by MesEmp desc ,AnnEmp desc
		end
		
			IF @Par_Aliado !='' and @Par_TipoProd='' and @Par_Producto=''
			begin 
				 select top 3 AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				  where MesEmp <=@Par_Mes  and AnnEmp = @Par_Anio and NomSuc=@Par_Sucursal and NomUser= @Par_Aliado
				  Group by AnnEmp,  MesEmp
				  --Order by AnnEmp desc, MesEmp desc 
				  Order by MesEmp desc ,AnnEmp desc
				  			 
			end
			
			IF @Par_Aliado ='' and @Par_TipoProd!='' and @Par_Producto=''
			begin 
				 select top 3 AnnEmp,  MesEmp, 
				 (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				  where MesEmp <=@Par_Mes  and AnnEmp = @Par_Anio and NomSuc=@Par_Sucursal and Tipo= @Par_TipoProd
				  Group by AnnEmp,  MesEmp
				  --Order by AnnEmp desc, MesEmp desc 
				  Order by MesEmp desc ,AnnEmp desc
				  				 
			end
			
			IF @Par_Aliado ='' and @Par_TipoProd='' and @Par_Producto!=''
			begin 
			
				select top 3 AnnEmp,  MesEmp, (select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				  where MesEmp <=@Par_Mes  and AnnEmp = @Par_Anio and NomSuc=@Par_Sucursal and Producto= @Par_Producto
				  Group by AnnEmp,  MesEmp
				  --Order by AnnEmp desc, MesEmp desc 
				  	Order by MesEmp desc ,AnnEmp desc		
			end
					
			IF @Par_Aliado !='' and @Par_TipoProd!='' and @Par_Producto!=''
			begin
			
				select top 3 AnnEmp,  MesEmp,
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				  where MesEmp <=@Par_Mes  and AnnEmp = @Par_Anio and NomSuc=@Par_Sucursal and
				  Producto= @Par_Producto and Tipo=@Par_TipoProd and NomUser= @Par_Aliado
				  Group by AnnEmp,  MesEmp
				  --Order by AnnEmp desc, MesEmp desc 
				  Order by MesEmp desc ,AnnEmp desc
				  
			end
			
			IF @Par_Aliado !='' and @Par_TipoProd!='' and @Par_Producto=''
			begin 
				
				select top 3 AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				  where MesEmp <=@Par_Mes  and AnnEmp = @Par_Anio and NomSuc=@Par_Sucursal and
				  Tipo=@Par_TipoProd and NomUser= @Par_Aliado
				  Group by AnnEmp,  MesEmp
				  --Order by AnnEmp desc, MesEmp desc 
				  Order by MesEmp desc ,AnnEmp desc
				  
			end	
			
			IF @Par_Aliado !='' and @Par_TipoProd='' and @Par_Producto!=''
			begin 
				
				select top 3 AnnEmp,  MesEmp,
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes, sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				  where MesEmp <=@Par_Mes  and AnnEmp = @Par_Anio and NomSuc=@Par_Sucursal and
				  NomUser= @Par_Aliado and Producto=@Par_Producto
				  Group by AnnEmp,  MesEmp
				  --Order by AnnEmp desc, MesEmp desc 
				  Order by MesEmp desc ,AnnEmp desc
				 
			end
				
			IF @Par_Aliado ='' and @Par_TipoProd!='' and @Par_Producto!=''
			begin 
				select top 3 AnnEmp,  MesEmp, 
				(select case MesEmp 
					when 1  then 'Enero-'
					when 2 then 'Febrero-'
					when 3 then 'Marzo-'
					when 4 then 'Abril-'
					when 5 then 'Mayo-'
					when 6 then 'Junio-'
					when 7 then 'Julio-'
					when 8 then 'Agosto-'
					when 9 then 'Septiembre-'
					when 10 then 'Octubre-'
					when 11 then 'Noviembre-'
					when 12 then 'Diciembre-' end
					) as NomMes,sum(Capital)/sum(cantidad)as Promedio from dbo.IndPrestamos 
				  where MesEmp <=@Par_Mes  and AnnEmp = @Par_Anio and NomSuc=@Par_Sucursal
				  and Tipo= @Par_TipoProd and Producto=@Par_Producto
				  Group by AnnEmp,  MesEmp
				  Order by MesEmp desc ,AnnEmp desc
				  
				
				 
			end
		END
		
		
	END
	

	
END


----- aliados
--SELECT NomUser, NomSuc, AnnEmp,SUM(capital)/SUM(cantidad) AS Promedio FROM  dbo.IndPrestamos
--WHERE
--NomSuc='Los Cocos' and
-- AnnEmp BETWEEN 2020-2 AND 2020 
--GROUP BY NomUser, AnnEmp, NomSuc, NomUser
--ORDER BY  NomUser ASC

-----productos




--SELECT p.NomUser, SUM(p.capital)/SUM(p.cantidad) AS PromedioMayor

--,(SELECT top 1 SUM(i.capital)/SUM(i.cantidad) AS Promedio FROM  dbo.IndPrestamos as i
--			WHERE i.AnnEmp = 2020-1 and i.NomUser=p.NomUser and i.NomSuc=p.NomSuc  
--			GROUP BY i.NomUser, i.AnnEmp, i.NomSuc
--			ORDER BY  i.NomUser ASC)AS PromedioMediano
			
--,(SELECT top 1 SUM(ind.capital)/SUM(ind.cantidad) AS Promedio, ind.AnnEmp FROM  dbo.IndPrestamos as ind
--			WHERE ind.AnnEmp = 2020-2 and ind.NomUser=p.NomUser and ind.NomSuc=p.NomSuc  
--			GROUP BY ind.NomUser, ind.AnnEmp, ind.NomSuc
--			ORDER BY  ind.NomUser ASC)AS PromedioMenor
			
--FROM  dbo.IndPrestamos as p
--			WHERE p.AnnEmp =2020  
--			and p.NomSuc='Centro 69' GROUP BY p.NomUser, p.AnnEmp, p.NomSuc
--			ORDER BY  p.NomUser ASC