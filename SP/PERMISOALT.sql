IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[DBO].[PERMISOALT]') AND type IN ('P'))
	DROP PROCEDURE PERMISOALT
GO

CREATE PROCEDURE PERMISOALT(
	------------------------------------------------------------------------------------------------------------------
	-- @nombre:			PERMISOALT
	-- @autor:			DINERO INMEDIATO
	-- @fecha:			19/03/2021
	-- @version:		1.0
	-- @descripcion:	Store procedure para dar de alta permiso dependiendo del puesto
	------------------------------------------------------------------------------------------------------------------|
	
	@Par_RolID					INT,
	@Par_PermisoID				INT,
	
	@Par_Salida					CHAR(1),				-- Parametro para salida de datos
	@Par_NumErr					CHAR(6) OUT,			-- Parametro de entrada/salida de numero de error
	@Par_ErrMenDev 				VARCHAR(800) OUT		-- Parametro de entrada/salida de mensaje de control de respuesta 
	--@Par_Consecutivo			INT	OUT				    -- Parametro de entrada/salida de para indicar el id que se a generado o actualizado	
)
AS

BEGIN

-- Declaracion de constantes 
	DECLARE @ENTERO_VACIO		INT,				-- Entero vacio
			@CADENA_VACIA		CHAR(1),			-- Cadena vacia
			@DECIMAL_VACIO		DECIMAL(9,2),			-- Cadena vacia
			@FECHA_VACIA		DATETIME,			-- Fecha vacia
			@SalidaSI			CHAR(1),			-- Salida si
			@SalidaNO			CHAR(1),			-- Salida no
			@CodigoExito		CHAR(6), 			-- Codigo exito			
			@Var_FolioID		INT
	
		-- Declaracion de variables 
	DECLARE	@Var_TablaID		INT,					-- FOLIO ID
			@Var_BaseReg		INT,					-- Se verifica si el cliente ya tiene una base
			@Var_MontoBase		DECIMAL(14,2),
			@Var_Fecha			DATETIME,
			@Var_Anio			INT
		
   
   
-- Asignacion de constantes 
	SELECT	@ENTERO_VACIO		=	0,				-- Asignacion de entero vacio
			@CADENA_VACIA		=	'Todos',				-- Asignacion de cadena vacia
			@DECIMAL_VACIO		=	0000000.00,		-- Asinacion de decimal vacio
			@FECHA_VACIA		=	'1900-01-01',	-- Asignacion de fecha vacia
			@SalidaSI			=	'S',			-- Asignacion de salida si
			@SalidaNO			=	'N',			-- Asignacion de salida no
			@CodigoExito		=	'000000'		-- Codigo exito


	SET	@Par_RolID					        = ISNULL(@Par_RolID					,@ENTERO_VACIO)	
	SET	@Par_PermisoID						= ISNULL(@Par_PermisoID				,@ENTERO_VACIO)		

	IF @Par_RolID				=   @ENTERO_VACIO	
	BEGIN
		SET	@Par_NumErr		= '000001'
		SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA EL PUESTO'
		GOTO ManejoErrores	
	END
		
	IF @Par_PermisoID			=   @ENTERO_VACIO	
	BEGIN
		SET	@Par_NumErr		= '000002'
		SET @Par_ErrMenDev	=	'ERROR!!, SE ESPERABA EL MODULO'
		GOTO ManejoErrores	
	END
	
				--select * from PermisoDenegadoRol
				--select * from ModuloPuesto
	--INSERT INTO PermisoDenegadoRol(RolID,		PermisoID)
	--          VALUES (@Par_RolID,				@Par_PermisoID)
 
	INSERT INTO ModuloPuesto(Pe_Cve_Puesto_Empleado,		ModuloID)
             VALUES (@Par_RolID,				@Par_PermisoID)
	
	SET @Par_NumErr  	= '000000'
	SET @Par_ErrMenDev	= 'SE HA REGISTRADO EL PERMISO CON �XITO'
	
	
	ManejoErrores:
			
	IF @Par_Salida	= @SalidaSI
	BEGIN

	
		SELECT	@Par_NumErr 		AS NumErr,
				@Par_ErrMenDev		AS ErrMenDev
				--@Par_Consecutivo	AS Consecutivo
	END
	

END
