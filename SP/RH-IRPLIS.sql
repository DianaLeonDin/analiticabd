IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[DBO].[IRPLIS]') AND type IN ('P'))
	DROP PROCEDURE IRPLIS
GO

CREATE PROCEDURE IRPLIS(
	------------------------------------------------------------------------------------------------------------------
	-- @nombre:			IRPLIS
	-- @autor:			DINERO INMEDIATO
	-- @fecha:			03/12/2020
	-- @version:		1.0
	-- @descripcion:	Store procedure para consultar informacion del indicador de rotacion de personal
	------------------------------------------------------------------------------------------------------------------|
		
	@Par_NumLis			INT 					-- Numero de lista
)
AS
BEGIN
	
	-- Declaracion de constantes 
	DECLARE @Var_ListaIRP		INT,
			@Var_ListaIRPAlta	INT,
			@Var_ListaIRPBaja	INT,
			@Var_ListaIRPAcum	INT

	-- Asignacion de constantes 
	SELECT	@Var_ListaIRP			=	1,
			@Var_ListaIRPAlta		=	2,
			@Var_ListaIRPBaja		=	3,
			@Var_ListaIRPAcum		=	4
			
			
	IF @Par_NumLis = @Var_ListaIRP
	BEGIN
	      			
		SELECT Ann, CAST(ISNULL(SUM(CASE mes WHEN 1 THEN oper1/((EmpAlt + Acumulado)/2) END),0) AS DECIMAL(7,2))  AS Ene, 
			CAST(ISNULL(SUM(CASE mes WHEN 2 THEN oper1/((EmpAlt + Acumulado)/2) END),0) AS DECIMAL(7,2))  AS Feb,
			CAST(ISNULL(SUM(CASE mes WHEN 3 THEN oper1/((EmpAlt + Acumulado)/2) END),0) AS DECIMAL(7,2)) AS Mar,
			CAST(ISNULL(SUM(CASE mes WHEN 4 THEN oper1/((EmpAlt + Acumulado)/2) END),0) AS DECIMAL(7,2)) AS Abr,
			CAST(ISNULL(SUM(CASE mes WHEN 5 THEN oper1/((EmpAlt + Acumulado)/2) END),0) AS DECIMAL(7,2)) AS May,
			CAST(ISNULL(SUM(CASE mes WHEN 6 THEN oper1/((EmpAlt + Acumulado)/2) END),0) AS DECIMAL(7,2)) AS Jun,
			CAST(ISNULL(SUM(CASE mes WHEN 7 THEN oper1/((EmpAlt + Acumulado)/2) END),0) AS DECIMAL(7,2)) AS Jul,
			CAST(ISNULL(SUM(CASE mes WHEN 8 THEN oper1/((EmpAlt + Acumulado)/2) END),0) AS DECIMAL(7,2)) AS Ago,
			CAST(ISNULL(SUM(CASE mes WHEN 9 THEN oper1/((EmpAlt + Acumulado)/2) END),0) AS DECIMAL(7,2)) AS Sep,
			CAST(ISNULL(SUM(CASE mes WHEN 10 THEN oper1/((EmpAlt + Acumulado)/2) END),0) AS DECIMAL(7,2)) AS Oct,
			CAST(ISNULL(SUM(CASE mes WHEN 11 THEN oper1/((EmpAlt + Acumulado)/2) END),0) AS DECIMAL(7,2)) AS Nov,
			CAST(ISNULL(SUM(CASE mes WHEN 12 THEN oper1/((EmpAlt + Acumulado)/2) END),0) AS DECIMAL(7,2)) AS Dic
			  FROM vwIndRecPer3
			  GROUP BY Ann

	END	
	
	
	IF @Par_NumLis = @Var_ListaIRPAlta
	BEGIN
	
		SELECT Ann,
			ISNULL(SUM(CASE mes WHEN 1 THEN EmpAlt END),0)  AS Ene,
			ISNULL(SUM(CASE mes WHEN 2 THEN EmpAlt END),0)  AS Feb,
			ISNULL(SUM(CASE mes WHEN 3 THEN EmpAlt END),0)  AS Mar,
			ISNULL(SUM(CASE mes WHEN 4 THEN EmpAlt END),0)  AS Abr,
			ISNULL(SUM(CASE mes WHEN 5 THEN EmpAlt END),0)  AS May,
			ISNULL(SUM(CASE mes WHEN 6 THEN EmpAlt END),0)  AS Jun,
			ISNULL(SUM(CASE mes WHEN 7 THEN EmpAlt END),0)  AS Jul,
			ISNULL(SUM(CASE mes WHEN 8 THEN EmpAlt END),0)  AS Ago,
			ISNULL(SUM(CASE mes WHEN 9 THEN EmpAlt END),0)  AS Sep,
			ISNULL(SUM(CASE mes WHEN 10 THEN EmpAlt END),0)  AS Oct,
			ISNULL(SUM(CASE mes WHEN 11 THEN EmpAlt END),0)  AS Nov,
			ISNULL(SUM(CASE mes WHEN 12 THEN EmpAlt END),0)  AS Dic
			  FROM vwIndRecPer3
			  GROUP BY Ann
  
	END
	
	
	IF @Par_NumLis = @Var_ListaIRPBaja
	BEGIN
			
			  SELECT Ann,
				ISNULL(SUM(CASE mes WHEN 1 THEN EmpBaj END),0)  AS Ene,
				ISNULL(SUM(CASE mes WHEN 2 THEN EmpBaj END),0)  AS Feb,
				ISNULL(SUM(CASE mes WHEN 3 THEN EmpBaj END),0)  AS Mar,
				ISNULL(SUM(CASE mes WHEN 4 THEN EmpBaj END),0)  AS Abr,
				ISNULL(SUM(CASE mes WHEN 5 THEN EmpBaj END),0)  AS May,
				ISNULL(SUM(CASE mes WHEN 6 THEN EmpBaj END),0)  AS Jun,
				ISNULL(SUM(CASE mes WHEN 7 THEN EmpBaj END),0)  AS Jul,
				ISNULL(SUM(CASE mes WHEN 8 THEN EmpBaj END),0)  AS Ago,
				ISNULL(SUM(CASE mes WHEN 9 THEN EmpBaj END),0)  AS Sep,
				ISNULL(SUM(CASE mes WHEN 10 THEN EmpBaj END),0) AS Oct,
				ISNULL(SUM(CASE mes WHEN 11 THEN EmpBaj END),0) AS Nov,
				ISNULL(SUM(CASE mes WHEN 12 THEN EmpBaj END),0) AS Dic
				  FROM vwIndRecPer3
				  GROUP BY Ann
  
	END

	
	IF @Par_NumLis = @Var_ListaIRPAcum
	BEGIN
	
		SELECT Ann,
			ISNULL(SUM(CASE mes WHEN 1 THEN Acumulado END),0)  AS Ene,
			ISNULL(SUM(CASE mes WHEN 2 THEN Acumulado END),0)  AS Feb,
			ISNULL(SUM(CASE mes WHEN 3 THEN Acumulado END),0)  AS Mar,
			ISNULL(SUM(CASE mes WHEN 4 THEN Acumulado END),0)  AS Abr,
			ISNULL(SUM(CASE mes WHEN 5 THEN Acumulado END),0)  AS May,
			ISNULL(SUM(CASE mes WHEN 6 THEN Acumulado END),0)  AS Jun,
			ISNULL(SUM(CASE mes WHEN 7 THEN Acumulado END),0)  AS Jul,
			ISNULL(SUM(CASE mes WHEN 8 THEN Acumulado END),0)  AS Ago,
			ISNULL(SUM(CASE mes WHEN 9 THEN Acumulado END),0)  AS Sep,
			ISNULL(SUM(CASE mes WHEN 10 THEN Acumulado END),0) AS Oct,
			ISNULL(SUM(CASE mes WHEN 11 THEN Acumulado END),0) AS Nov,
			ISNULL(SUM(CASE mes WHEN 12 THEN Acumulado END),0) AS Dic
			  FROM vwIndRecPer3
			  GROUP BY Ann
  
	END
	
	
	
END
