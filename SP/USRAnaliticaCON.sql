IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[DBO].[USRINDCON]') AND type IN ('P'))
	DROP PROCEDURE USRINDCON
GO

CREATE PROCEDURE USRINDCON(
	------------------------------------------------------------------------------------------------------------------
	-- @nombre:			USRINDCON
	-- @autor:			DINERO INMEDIATO
	-- @fecha:			03/03/2021
	-- @version:		1.0
	-- @descripcion:	Store procedure para EL LOGIN DEL PROYECTO DE INDICADORES
	------------------------------------------------------------------------------------------------------------------|
	@Par_Usr					VARCHAR(30),
	@Par_Password				VARCHAR(200),
	@Par_numCon					INT
)
AS
BEGIN

	-- Declaracion de constantes 
	DECLARE @ENTERO_VACIO		INT,				-- Entero vacio
			@DECIMAL_VACIO		DECIMAL(8,2),		-- Decimal vacio
			@CADENA_VACIA		CHAR(1),			-- Cadena vacia
			@FECHA_VACIA		DATETIME,			-- Fecha vacia
			@SalidaSI			CHAR(1),			-- Salida si
			@SalidaNO			CHAR(1),			-- Salida no
			@CodigoExito		CHAR(6),			-- Codigo exito		
			@Var_Es_Cve_Estado	CHAR(6),		
			--consultar ticket por estados
			@Var_Con_Login		INT
			

	-- Declaracion de variables 
	DECLARE	@Var_MenPersUno		VARCHAR(100)		--  Mensaje de personalizado para la tabla de MENSAJESISTEMA para el campo MensajeDev

			
	-- Asignacion de constantes 
	SELECT	@ENTERO_VACIO			=	0,				-- Asignacion de entero vacio
			@DECIMAL_VACIO			=	0.0,
			@CADENA_VACIA			=	'',				-- Asignacion de cadena vacia
			@FECHA_VACIA			=	'1900-01-01',	-- Asignacion de fecha vacia
			@SalidaSI				=	'S',			-- Asignacion de salida si
			@SalidaNO				=	'N',			-- Asignacion de salida no
			@CodigoExito			=	'000000',		-- Codigo exito
			@Var_Con_Login			=   1,
			@Var_Es_Cve_Estado		=	'AC'
			
		-------------------		VERIFICAR QUE EXISTA EL NUMERO DE IMSS	------------------- 
		
		IF	@Par_numCon =	@Var_Con_Login
		BEGIN
			
		--SELECT Em_Cve_Empleado, Em_Num_IMSS, Em_Email 
		--	FROM DBDIN.dbo.Empleado AS E WHERE
		--	E.Em_Num_IMSS=@Par_NumImss
		--	AND e.Es_Cve_Estado=@Var_Es_Cve_Estado
		
		SELECT E.Em_Cve_Empleado,E.Em_UserDef_1, E.Em_Password , E.Em_Nombre, P.Pe_Cve_Puesto_Empleado ,P.Pe_Descripcion
			FROM DBDIN.dbo.Empleado AS E
			INNER JOIN DBDIN.dbo.Puesto_Empleado as P 
			ON E.Pe_Cve_Puesto_Empleado = P.Pe_Cve_Puesto_Empleado WHERE
			E.Em_UserDef_1=@Par_Usr
			AND Em_Password=@Par_Password
			AND e.Es_Cve_Estado=@Var_Es_Cve_Estado

			
		END
		
END

