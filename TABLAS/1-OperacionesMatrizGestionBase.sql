
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[DBO].[BASE]') AND type IN ('U'))
	DROP TABLE BASE
GO

CREATE TABLE BASE(
--CATALOGO MANTENIMINETO... MANTIENE INFORMACION DE LA BASE DE LA MATRIZ DE INDICADORES DE GESTION DEL AREA DE OPERACIONES
BaseID 				INT			        NOT	NULL IDENTITY(1,1),
NomIndicador		VARCHAR(200) 	    NOT NULL,
ValBase				DECIMAL(9,2)				    NOT NULL,--INT
--Frecuencia			VARCHAR(200) 	    NOT NULL,
SucursalID			VARCHAR(200) 	    NOT NULL,

AliadoID			VARCHAR(300) 	    NOT NULL,	--valuador, empleado
Mes					INT			 	    NOT NULL,	
anio				INT			 	    NOT NULL,

--ELIMINAR
Activo 			    BIT 			    NOT NULL
CONSTRAINT PK_BASE PRIMARY KEY(BaseID)
);



--IDIndicador
--NomIndicador
--ValBase
--Frecuencia
--Sucursal
