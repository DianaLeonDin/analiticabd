
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[D-BO].[ModuloPuesto]') AND type IN ('U'))
	DROP TABLE ModuloPuesto
GO

CREATE TABLE ModuloPuesto(
--CATALOGO ModuloPuesto... MANTIENE ID DE LA TABLA DE PUESTO Y DE MODULO PARA ENLAZARLAS PARA LOS INDICADORES
ModuloPuestoID 				INT			        NOT	NULL IDENTITY(1,1),
Pe_Cve_Puesto_Empleado		VARCHAR(10)	        NOT	NULL,
SubModuloID 				INT					NOT	NULL
CONSTRAINT PK_ModuloPuesto PRIMARY KEY(ModuloPuestoID)
);