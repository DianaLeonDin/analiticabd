
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[D-BO].[PermisoOperaciones]') AND type IN ('U'))
	DROP TABLE PermisoOperaciones
GO

CREATE TABLE PermisoOperaciones(
--CATALOGO Modulo... MANTIENE INFORMACION DE LOS MODULOS QUE EXISTEN EN EL MENU LATERAL DE LOS INDICADORES
PermisoOpID				INT			        NOT	NULL IDENTITY(1,1),
Sucursal				VARCHAR(300)        NOT	NULL,
SucursalID				VARCHAR(300)        NOT	NULL,
UsuarioID	 			VARCHAR(200)        NOT	NULL
CONSTRAINT PK_PermisoOperaciones PRIMARY KEY(PermisoOpID)
);

