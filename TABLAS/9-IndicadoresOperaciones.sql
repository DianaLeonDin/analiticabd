
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[D-BO].[IndOperaciones]') AND type IN ('U'))
	DROP TABLE IndOperaciones
GO

CREATE TABLE IndOperaciones(
--... MANTIENE INFORMACION DE LOS INDICADORES PARA OPERACIONES
IndicadorID				INT					 NOT	NULL IDENTITY(1,1),
Nombre					VARCHAR(400)         NOT	NULL,
--Meta					DECIMAL(12,2)        NOT	NULL,
Real					DECIMAL(12,2)        NOT	NULL,
--Avance				DECIMAL(12,2)        NOT	NULL,
--Tendencia				DECIMAL(12,2)        NOT	NULL,
--Cumplimiento			DECIMAL(12,2)        NOT	NULL,
Fecha					DATETIME	         NOT	NULL,
SucursalID				VARCHAR(300)	     NOT	NULL,
StsEnvio				SMALLINT			 NOT	NULL,
RegVersion				SMALLINT			 NOT	NULL,
IdFilaLocal				INT					 NOT	NULL,
IdFilaCentral			INT					 NOT	NULL,
FecRevCentral			DATETIME			 NOT	NULL,
FecRevLocal				DATETIME			 NOT	NULL,
CveSuc					INT					 NOT	NULL
CONSTRAINT PK_IndOperaciones PRIMARY KEY(IndicadorID)
);

--DROP TABLE IndicadoresOperaciones